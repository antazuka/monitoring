// required mootools

//var AjaxSessionAlive = 0;
var AjxKeepAliveTimer = 5*60*1000;
var KeepAliveRetry = 0;

//if (window.console && window.console.firebug) {
//	alert( 'firebug found!');
	//Firebug is known to make Gmail slow unless it is configured correctly.
//}
/*
 * string function to convert html string to element
 * http://mootools-users.660466.n2.nabble.com/Moo-New-element-from-html-string-td3557543.html
 */
/*
String.implement({ 
	toElement: function() { 
		return new Element('div', {html:this}).getFirst(); 
		} 
}); 
*/
function CurrencyFormat(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function GetUTCString(){
	var d = new Date();
	var e = Date.UTC(d.getFullYear(),d.getMonth(),d.getDay(),d.getHours(),d.getMinutes(),d.getSeconds(),d.getMilliseconds());
	return String(e);
}

function GetBooleanValue(test){
	if(test.toLowerCase()=='true' || test.toLowerCase()=='1')
		return true;
	else return false;
}

function MessegeInfoShow(msg,timer){
	timer = typeof timer !== 'undefined' ? timer : 7000;
	$('hde-messege-info').set('html',msg);
	$('hde-messege-info').setStyle('display','block');
	setTimeout('MessegeInfoHide()', timer); 
}

function MessegeInfoHide(){
	$('hde-messege-info').setStyle('display','none');
}

function KeepAlive(){
	Anov.Administration.Security.SessionAlive.AjxKeepAlive($('hdfe_USER_SID').get('value'), 
		function(AjxResp){
	        if($chk(AjxResp.value)){
				setTimeout('KeepAlive()', AjxKeepAliveTimer); 
			} else {
	            if(AjxResp.error){ 
					new Message({
						iconPath: $('hdfe_ApplicationPath').get('value')+'/images/icons/',
						icon: 'cautionMedium.png',
						title: 'Warning',
						fxOutDuration: 'long',
						fxDuration: 'long',
						message: 'SessionAlive.AjxKeepAlive : '+AjxResp.error.Type + ' : <br>' + AjxResp.error.Message
					}).say();						
					KeepAliveRetry = 0;
					setTimeout('KeepAlive()', AjxKeepAliveTimer);					
				} else { 
                	if(KeepAliveRetry <5){
						KeepAliveRetry++;
						setTimeout('KeepAlive()', AjxKeepAliveTimer);
					} else {					
						new Message({
							iconPath: $('hdfe_ApplicationPath').get('value')+'/images/icons/',
							icon: 'cautionMedium.png',
							title: 'Warning',
							fxOutDuration: 'long',
							fxDuration: 'long',
							message: 'Invalid server response due to connection loss.<br> If this messege appears often, Please Reload the page'
						}).say();						
						KeepAliveRetry = 0;
						setTimeout('KeepAlive()', AjxKeepAliveTimer);
					}
				}
			}
	        AjxResp = null;
		}

	);
}

function GetInfoTest(){
	alert("GetInfoTest has been disabled");
	/*
	Anov.Administration.Security.SessionAlive.GetInfoTest($('hdfe_USER_UID').get('value'), 
		function(AjxResp){
			alert(AjxResp.value);
	        AjxResp = null;
		}

	);
	*/
}

function findSWF(movieName) {
	if (navigator.appName.indexOf("Microsoft")!= -1) {
		//return window["ie_" + movieName];
		return document.getElementById(movieName);
	} else {
		//return document[movieName];
		return document.getElementById(movieName);
	}
}

function SetDataGridPagingInfo(aDataGridMaxRow,aDataGridPageNum,aDataGridRowsPerPage){
	/*
	var myString = '{subject} is {property_1} and {property_2}.';
	var myObject = {subject: 'Jack Bauer', property_1: 'our lord', property_2: 'savior'};
	myString.substitute(myObject); //Jack Bauer is our lord and savior
	*/	
    var chk
    if (aDataGridMaxRow>(aDataGridRowsPerPage * aDataGridPageNum))
    	chk = (aDataGridRowsPerPage * aDataGridPageNum);
	else chk = aDataGridMaxRow;
	return '<b>' + ((aDataGridRowsPerPage * (aDataGridPageNum-1)).toFloat()+ 1)  + '</b> - <b>' + chk + '</b> of <b>' + aDataGridMaxRow + '</b>' ;
}
function SetDataGridPagingInfoAfterInsert(aDataGridMaxRow,aDataGridPageNum,aDataGridRowsPerPage,aDataGridPageNumMax,navElmCurrent1,navElmCurrent2,aDataGridAddedRow){
	/*
	var myString = '{subject} is {property_1} and {property_2}.';
	var myObject = {subject: 'Jack Bauer', property_1: 'our lord', property_2: 'savior'};
	myString.substitute(myObject); //Jack Bauer is our lord and savior
	*/	
    var chk;
    var res;
    if(aDataGridPageNumMax == 1){
		chk = aDataGridMaxRow;
        res = '<b>' + ((aDataGridRowsPerPage * (aDataGridPageNum-1)).toFloat()+ 1)  + '</b> - <b>' + chk + '</b> of <b>' + aDataGridMaxRow + '</b>' ;
	} else if(aDataGridPageNumMax > 1){
    	chk = (aDataGridRowsPerPage * aDataGridPageNum);
        res = '<b>' + ((aDataGridRowsPerPage * (aDataGridPageNum-1)).toFloat()+ 1)  + '</b> - <b>' + chk + '+(' + aDataGridAddedRow + ')</b> of <b>' + aDataGridMaxRow + '</b>' ;
	}
    navElmCurrent1.set('html',res);
	if($defined(navElmCurrent2))navElmCurrent2.set('html',navElmCurrent1.get('html'));  
}

function SetDataGridPagingNavControls(
	DataGridMaxRow, 
	DataGridPageNum, 
	DataGridRowsPerPage,
    DataGridPageNumMax,
	navElmCurrent1,
	navElmFirst1,
    navElmPrev1,
	navElmNext1,
    navElmLast1,
	navElmCurrent2,
	navElmFirst2,
    navElmPrev2,
	navElmNext2,
    navElmLast2
	){
    if(DataGridMaxRow != 0){
        if(DataGridPageNum == 1){
        	navElmCurrent1.set('html',SetDataGridPagingInfo(DataGridMaxRow,DataGridPageNum,DataGridRowsPerPage));
	        navElmFirst1.setStyle('display', 'none'); 
	        navElmPrev1.setStyle('display', 'none'); 
        	if(DataGridPageNumMax==1){
		        navElmNext1.setStyle('display', 'none'); 
		        navElmLast1.setStyle('display', 'none'); 
        	} else if(DataGridPageNumMax==2){
		        navElmNext1.setStyle('display', 'inline'); 
		        navElmLast1.setStyle('display', 'none'); 
        	} else if(DataGridPageNumMax>2){
		        navElmNext1.setStyle('display', 'inline'); 
		        navElmLast1.setStyle('display', 'inline'); 
        	} 
		} else if(DataGridPageNum > 1 && DataGridPageNum < DataGridPageNumMax){
        	navElmCurrent1.set('html',SetDataGridPagingInfo(DataGridMaxRow,DataGridPageNum,DataGridRowsPerPage));
        	if(DataGridPageNum==2){
                if(DataGridPageNum == (DataGridPageNumMax-1)){
		        	navElmFirst1.setStyle('display', 'none'); 
			        navElmPrev1.setStyle('display', 'inline'); 
			        navElmNext1.setStyle('display', 'inline'); 
			        navElmLast1.setStyle('display', 'none'); 
				} else {
		        	navElmFirst1.setStyle('display', 'none'); 
			        navElmPrev1.setStyle('display', 'inline'); 
			        navElmNext1.setStyle('display', 'inline'); 
			        navElmLast1.setStyle('display', 'inline'); 
				}
        	} else if(DataGridPageNum==(DataGridPageNumMax-1)){               
	        	navElmLast1.setStyle('display', 'none'); 
		        navElmFirst1.setStyle('display', 'inline'); 
		        navElmPrev1.setStyle('display', 'inline'); 
		        navElmNext1.setStyle('display', 'inline'); 
        	} else if(DataGridPageNum>2 && DataGridPageNum<(DataGridPageNumMax-1)){               
		        navElmFirst1.setStyle('display', 'inline'); 
		        navElmPrev1.setStyle('display', 'inline'); 
		        navElmNext1.setStyle('display', 'inline'); 
		        navElmLast1.setStyle('display', 'inline'); 
        	} 	
		} else if (DataGridPageNum == DataGridPageNumMax){
            if(DataGridPageNum==2){
	        	navElmCurrent1.set('html',SetDataGridPagingInfo(DataGridMaxRow,DataGridPageNum,DataGridRowsPerPage));
		        navElmFirst1.setStyle('display', 'none'); 
		        navElmPrev1.setStyle('display', 'inline'); 
		        navElmNext1.setStyle('display', 'none'); 
		        navElmLast1.setStyle('display', 'none'); 
			} else {
	        	navElmCurrent1.set('html',SetDataGridPagingInfo(DataGridMaxRow,DataGridPageNum,DataGridRowsPerPage));
		        navElmFirst1.setStyle('display', 'inline'); 
		        navElmPrev1.setStyle('display', 'inline'); 
		        navElmNext1.setStyle('display', 'none'); 
		        navElmLast1.setStyle('display', 'none'); 
			}
		}
	} else {
        // no data 
        navElmCurrent1.set('html',''); 
        navElmFirst1.setStyle('display', 'none'); 
        navElmPrev1.setStyle('display', 'none'); 
        navElmNext1.setStyle('display', 'none'); 
        navElmLast1.setStyle('display', 'none'); 
	}
    
	if($defined(navElmCurrent2))navElmCurrent2.set('html',navElmCurrent1.get('html'));  
	if($defined(navElmFirst2))navElmFirst2.setStyle('display', navElmFirst1.getStyle('display')); 
	if($defined(navElmPrev2))navElmPrev2.setStyle('display', navElmPrev1.getStyle('display')); 
	if($defined(navElmNext2))navElmNext2.setStyle('display', navElmNext1.getStyle('display')); 
	if($defined(navElmLast2))navElmLast2.setStyle('display', navElmLast1.getStyle('display')); 
}   

function GetInputLabel(inputElement){
    	return '`'+inputElement.getPrevious().get('text').replace('*','').replace(' :','').replace(':','')+'`';
}
function goNextAnchor(where) {
	//return;
    // still buggy
     //window.location.hash = '';
     window.location.hash = where;
	// return false;
}

function GetFilenameFromUrl(url){
	var filename = url.substring(url.lastIndexOf('/')+1);
	var arr = filename.split('?');
	return arr[0];
}

function deltmpfile(filename){
	if(filename){
		if(filename!=''){
			if($chk($('frm-CLNSVC'))){
				$('frm-CLNSVC').set('src','Common/ClnSvc.aspx?tp=sf&q1='+filename);
			}		
		}
	}
}

function deltmpfiles(usersid){
	if(usersid){
		if(usersid!=''){
			if($chk($('frm-CLNSVC'))){
				$('frm-CLNSVC').set('src','Common/ClnSvc.aspx?tp=mf&q1='+usersid);
			}		
		}
	}
}

// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place
// onKeyPress='return InputNumbersOnly(this, event)'
function InputNumbersOnly(myfield, e, dec){
	var key;
	var keychar;

	if (window.event)
	   key = window.event.keyCode;
	else if (e)
	   key = e.which;
	else
	   return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || 
	    (key==9) || (key==13) || (key==27) )
	   return true;

	// numbers
	else if ((('0123456789').indexOf(keychar) > -1))
	   return true;

	// decimal point jump
	else if (dec && (keychar == '.')) {
	   myfield.form.elements[dec].focus();
	   return false;
	}
	else return false;
}

function InputTextNumbersOnly(myfield, e, dec){
	var key;
	var keychar;

	if (window.event)
	   key = window.event.keyCode;
	else if (e)
	   key = e.which;
	else
	   return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || 
	    (key==9) || (key==13) || (key==27) )
	   return true;

	// numbers
	else if ((('0123456789').indexOf(keychar) > -1))
	   return true;

	// decimal point jump
	else if (dec && (keychar == '.'))
	   {
	   myfield.form.elements[dec].focus();
	   return false;
	   }
	else
	   return false;
}
// untuk date field
function InputSlashNumbersOnly(myfield, e, dec){
	var key;
	var keychar;

	if (window.event)
	   key = window.event.keyCode;
	else if (e)
	   key = e.which;
	else
	   return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || 
	    (key==9) || (key==13) || (key==27) )
	   return true;

	// numbers
	else if ((('0123456789/').indexOf(keychar) > -1))
	   return true;

	// decimal point jump
	else if (dec && (keychar == '.'))
	   {
	   myfield.form.elements[dec].focus();
	   return false;
	   }
	else
	   return false;
}
// untuk float field
function InputFloatNumbersOnly(myfield, e, dec){
	var key;
	var keychar;

	if (window.event)
	   key = window.event.keyCode;
	else if (e)
	   key = e.which;
	else
	   return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || 
	    (key==9) || (key==13) || (key==27) )
	   return true;

	// numbers
	else if ((('0123456789.').indexOf(keychar) > -1))
	   return true;

	// decimal point jump
	else if (dec && (keychar == '.'))
	   {
	   myfield.form.elements[dec].focus();
	   return false;
	   }
	else
	   return false;
}

	//onfocus="FormattedUnSet(this)" 
	function FormattedUnSet(elm){
		elm.set('value',elm.get('clvalue'));
	}
	
	//onblur="FormattedSet(this)"
	function FormattedSet(elm){
		if(elm.get('value')==''){
			elm.set('clvalue','0');
			elm.set('clvalue','0');
		} else {
			elm.set('clvalue',elm.get('value'));
			elm.set('value',FormatCurrency(elm.get('clvalue')));
		}
	}
	function FormatCurrency(nStr){
		nStr += '';
			x = nStr.split('.');
			x1 = x[0];
			x2 = x.length > 1 ? '.' + x[1] : '';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) {
				x1 = x1.replace(rgx, '$1' + '.' + '$2');
			}
			return x1 + x2;
	}

////****************************************************************
//// function untuk Get URL Parameters
//// http://www.netlobo.com/url_query_string_javascript.html
////****************************************************************
function gup( name ){
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}
