
var dg1;
var dg1_Xml;
var dgbox1_Title = 'Register Module ';
var dgbox1_RowsPerPage = 5;
var dgbox1_PageNum = 1;
var dgbox1_PageNumMax = 1;
var dgbox1_MaxRow = 0;
var dgbox1_NoRow = false;
var dgbox1_UiState; //1=insert; 2=update
var dgbox1_AddedRow = 0;
var dgbox1_AddSender = 0; //1=top; 2=bottom
var dgbox1_CurrentRecNum = -1;
var dgbox1_CurrentNav = 0;
var dgbox1_SearchType = 1; // 1=normal search, 2=advanced
var ce1;
var ce2;
var bodyscroll;
var gTips;

bodyscroll = new Fx.Scroll(document.body, {
		wait: false,
		duration: 500,
		offset: {'x': 0, 'y': 0},
		transition: Fx.Transitions.Quart.easeOut 
	});	
document.title = 'AGTS - '+ dgbox1_Title;
InitialComponent();


function InitialComponent()
{
	$('bt-search-go').addEvent('click', function(e){	
		//var tes = $('inp-dgbox1-normal-search-q').get('value');
		dg1__Refresh(dgbox1_PageNum);
		
	});
	$('bt-reset-go').addEvent('click', function(e){	
		$('inp-dgbox1-normal-search-q').set('value','');
		dg1__Refresh(dgbox1_PageNum);
	});
	
	
	$('bt-dg1-refresh-1').addEvent('click', function(e){	
		dg1__Refresh(dgbox1_PageNum);
	});
	
	$('bt-dgbox1-grid-select-1a').addEvent('click', function(e,c){
		//var checked = $('bt-dgbox1-grid-select-2a').set('checked',this.checked);
		if($defined(dg1)){
			if(this.checked == true) dg1.setCheckedRows(0,1);
			else dg1.setCheckedRows(0,0);
		}
            	
	});	
	
	// button add
	$('bt-dg1-add-1').addEvent('click', function(e){	
        $('inp-module_uid').set('value','');
		dg1__SwithcUIModf(1);
	});
	
	$('bt-dgbox1-editor-save-1').addEvent('click', function(e){ 

		if(dgbox1_UiState == 1) { dg1__Save(); }
		else if(dgbox1_UiState == 2) { dg1__Update(); }
			
	});
	
	//multi
	$('bt-dg1-delete-multi-1').addEvent('click', function(e){
		var CheckedRows = dg1.getCheckedRows(0);
		if($chk(CheckedRows)){
			if(confirm('Are you sure to delete selected row(s)?')){
				//alert(CheckedRows);
				dg1__Delete(String(CheckedRows));
			};
		}
	});
		
	$('bt-dgbox1-editor-delete-single-1').addEvent('click', function(e){	
		var CheckedRows = $('inp-module_uid').get('value');   // Codegen JS#00 Manually
		if($chk(CheckedRows)){
			if(confirm('Are you sure to delete current data?')){
				try {
					dg1__SwithcUIModf(0);						
				} finally {
					dg1__Delete(CheckedRows);
				}										
			};
		}
	});
	
	//back to grid
	$('bt-dgbox1-editor-cancel-1').addEvent('click', function(e){ 
			dg1__SwithcUIModf(0);
			if(dgbox1_CurrentRecNum == 0){
				if(dgbox1_AddSender==1){
					setTimeout("bodyscroll.toTop()", 255);
				} else if(dgbox1_AddSender==2){
					setTimeout("bodyscroll.toBottom()", 255);
				} 
			} else {
				setTimeout("bodyscroll.start(0,$('dg1-row-'+dgbox1_CurrentRecNum).getPosition().y)", 255);			
			}
	});
	
	//paging nav
	$('bt-dgbox1-nav-first-1').addEvent('click', function(e){ 
		dg1__Refresh(1);
	});
	//$('bt-dgbox1-nav-first-2').cloneEvents($('bt-dgbox1-nav-first-1'));
	$('bt-dgbox1-nav-prev-1').addEvent('click', function(e){ 
		dg1__Refresh(dgbox1_PageNum-1);
	});
	//$('bt-dgbox1-nav-prev-2').cloneEvents($('bt-dgbox1-nav-prev-1'));
	$('bt-dgbox1-nav-next-1').addEvent('click', function(e){ 
		dg1__Refresh(dgbox1_PageNum+1);
	});
	//$('bt-dgbox1-nav-next-2').cloneEvents($('bt-dgbox1-nav-next-1'));
	$('bt-dgbox1-nav-last-1').addEvent('click', function(e){ 
		dg1__Refresh(dgbox1_PageNumMax);
	});
	//$('bt-dgbox1-nav-last-2').cloneEvents($('bt-dgbox1-nav-last-1'));

	
	InitialGrid();

}

function InitialGrid()
{
	
	try {
		dg1 = new dhtmlXGridObject('dgbox1');
		dg1.setImagePath('images/dhtmlxGrid/');
		// #region Codegen : JS#00 Manually
		dg1.setHeader(',No,Module ID,Module Name,Active,Sort Order,Description,Date of Record');
		dg1.setInitWidths('20,35,150,250,150,150,*,200');
		dg1.setColAlign('right,center,left,left,left,left,left,left');
		dg1.setColVAlign("middle,middle,middle,middle,middle,middle,middle,middle");
		dg1.setColTypes('ch,ro,ro,ro,ro,ro,ro,ro');
		dg1.setColSorting('int,int,str,str,str,str,str,na');
		dg1.enableResizing('false,true,true,true,true,true,true,true');
		dg1.enableTooltips('false,false,false,false,false,false,false,false');
		// #endregion Codegen : JS#00 Manually
		dg1.enableAutoHeight(true);
		dg1.enableMultiline(true);
		dg1.enableColSpan(true);
		dg1.setSkin('clear'); 
		dg1.attachEvent('onRowSelect', function doOnRowSelected(id,ind){
			if(id!='*' && ind!=0 && ind!=7){
				if(!$chk($('inp-module_uid'))) return; // Codegen JS#00 Manually
				$('inp-module_uid').set('value',id);
				$('inp-module_id').set('value',dg1.getUserData(id,'module_id'));
				$('inp-module_name').set('value',dg1.getUserData(id,'module_name'));
				$('inp-sort_order').set('value',dg1.getUserData(id,'sort_order'));
				$('inp-isactive').set('value',dg1.getUserData(id,'isactive'));
				$('inp-description').set('value',dg1.getUserData(id,'description'));
				// #endregion Codegen : JS#01
				$('inp-create_by').set('value',dg1.getUserData(id,'create_by'));
				$('inp-create_byfn').set('text',dg1.getUserData(id,'create_byfn'));
				$('inp-create_at').set('text',dg1.getUserData(id,'create_date'));
				$('inp-create_by').set('value',dg1.getUserData(id,'update_by'));
				$('inp-update_byfn').set('text',dg1.getUserData(id,'update_byfn'));
				$('inp-update_at').set('text',dg1.getUserData(id,'update_date'));
				
				
				dg1__SwithcUIModf(2); 
			}
		});
		dg1.attachEvent('onCheckbox', function doOnCheck(rowId, cellInd, state) {                
			if(dgbox1_NoRow==true) return false;
			 else return true;
		});
		dg1.attachEvent("onRowAdded", function(rId){
			 
		}); 
		dg1.init();   
	}
	finally {
		dg1__Refresh(dgbox1_PageNum);
	}
	
}

function dg1__Refresh(pagenumber)
{
	
	var myRequest = new Request({
		url: regmodule_getByPage,
		data : {
			_token : $('_token').get('value'),
			rowsperpage : dgbox1_RowsPerPage,
			pagenum : pagenumber,
			//curentnav : dgbox1_CurrentNav,
			search : $('inp-dgbox1-normal-search-q').get('value')
		},
		method: 'get',
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none')
				$('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				dgbox1_MaxRow = response.maxrow;
				dgbox1_NoRow = response.norow;
				dgbox1_PageNum = response.pagenum;
				if(dgbox1_NoRow)dg1.setColumnHidden(0,true);else dg1.setColumnHidden(0,false); 

				var tmpMaxPage = (response.maxrow/dgbox1_RowsPerPage);
				if(tmpMaxPage > tmpMaxPage.round(0)) dgbox1_PageNumMax = tmpMaxPage.round(0) + 1; else dgbox1_PageNumMax = tmpMaxPage.round(0);
				dg1.clearAll();
				dg1.parse(response.data, function(grid_obj,count){
	                 SetDataGridPagingNavControls(
							dgbox1_MaxRow, dgbox1_PageNum, dgbox1_RowsPerPage, dgbox1_PageNumMax,
							$('bt-dgbox1-nav-current-1'), $('bt-dgbox1-nav-first-1'), $('bt-dgbox1-nav-prev-1'), $('bt-dgbox1-nav-next-1'), $('bt-dgbox1-nav-last-1'),
							$('bt-dgbox1-nav-current-2'), $('bt-dgbox1-nav-first-2'), $('bt-dgbox1-nav-prev-2'), $('bt-dgbox1-nav-next-2'), $('bt-dgbox1-nav-last-2')
					);
					dgbox1_AddedRow = 0;
					//delete loaded xml file after successfull loads
					
			     });
				 //dgbox1_CurrentNav = ((dgbox1_RowsPerPage * (dgbox1_PageNum-1)).toFloat()+ 1);
			}
			//$('inp-dgbox1-normal-search-q').set('value',response.module_name);
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}


function dg1__Save()
{
	
	var myRequest = new Request({
		url: regmodule_AjaxIns,
		data : {
			_token : $('_token').get('value'),
			module_uid : $('inp-module_uid').get('value'),
			module_id : $('inp-module_id').get('value'),
			module_name : $('inp-module_name').get('value'),
			sort_order : $('inp-sort_order').get('value'),
			isactive : $('inp-isactive').get('value'),
			description : $('inp-description').get('value')
		},
		method: 'post',
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none') $('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				
				if(dgbox1_MaxRow==0){								
					dg1__SwithcUIModf(0); 
					dg1__Refresh(dgbox1_PageNum); 
					Resp = null; 				
				}
				dg1.addRow(response.ud_module_uid,response.data.split('#*#'),0);   
				dg1.setUserData(response.ud_module_uid,'module_uid',response.ud_module_uid);
				dg1.setUserData(response.ud_module_uid,'module_id',response.ud_module_id);
				dg1.setUserData(response.ud_module_uid,'module_name',response.ud_module_name);
				dg1.setUserData(response.ud_module_uid,'sort_order',response.ud_sort_order);
				dg1.setUserData(response.ud_module_uid,'isactive',response.ud_isactive);
				dg1.setUserData(response.ud_module_uid,'description',response.ud_description);
				
				if(dgbox1_PageNumMax == 0) dgbox1_PageNumMax = 1;
				dgbox1_MaxRow = dgbox1_MaxRow+1;
				dgbox1_AddedRow = dgbox1_AddedRow + 1;
				SetDataGridPagingInfoAfterInsert(dgbox1_MaxRow,dgbox1_PageNum,dgbox1_RowsPerPage,dgbox1_PageNumMax,$('bt-dgbox1-nav-current-1'),$('bt-dgbox1-nav-current-2'),dgbox1_AddedRow);
				
				dg1.selectRow(0,false,false,false);
				dg1__SwithcUIModf(0);	
				setTimeout("bodyscroll.toTop()", 255);		
				MessegeInfoShow('Data successfully saved');					
			} 
			else 
			{ alert(response.ErrorMessege); }
			
			$('inp-dgbox1-normal-search-q').set('value',response.module_name);
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}

function dg1__Update()
{
	
	var myRequest = new Request({
		url: regmodule_AjaxUpd,
		data : {
			_token : $('_token').get('value'),
			module_uid : $('inp-module_uid').get('value'),
			module_id : $('inp-module_id').get('value'),
			module_name : $('inp-module_name').get('value'),
			sort_order : $('inp-sort_order').get('value'),
			isactive : $('inp-isactive').get('value'),
			description : $('inp-description').get('value'),
			rowidx : dgbox1_CurrentRecNum
		},
		method: 'post',
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none') $('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				
				var idx = dg1.getRowIndex(response.ud_module_uid); 
				if(idx == -1){alert('row not found');return;}
				dg1.deleteRow(response.ud_module_uid);
				dg1.addRow(response.ud_module_uid,response.data.split('#*#'),idx);   
				
				dg1.setUserData(response.ud_module_uid,'module_uid',response.ud_module_uid);
				dg1.setUserData(response.ud_module_uid,'module_id',response.ud_module_id);
				dg1.setUserData(response.ud_module_uid,'module_name',response.ud_module_name);
				dg1.setUserData(response.ud_module_uid,'sort_order',response.ud_sort_order);
				dg1.setUserData(response.ud_module_uid,'isactive',response.ud_isactive);
				dg1.setUserData(response.ud_module_uid,'description',response.ud_description);
				
				dg1.selectRow(idx,false,false,false);
				dg1__SwithcUIModf(0);	
				setTimeout("bodyscroll.toTop()", 255);		
				MessegeInfoShow('Data successfully saved');					
			} 
			else 
			{ alert(response.ErrorMessege); }
			
			$('inp-dgbox1-normal-search-q').set('value',response.module_name);
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}

function dg1__Delete(CheckedRows)
{
	var myRequest = new Request({
		url: regmodule_AjaxDel,
		method: 'delete',
		data : {
			_token : $('_token').get('value'),
			module_uid : CheckedRows
		},
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none') $('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				dg1__Refresh(dgbox1_PageNum);
				setTimeout("bodyscroll.toTop()", 255);
			} 
			else 
			{ alert(response.ErrorMessege); }
			
			$('inp-dgbox1-normal-search-q').set('value',response.module_name);
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}


function dg1__SwithcUIModf(UIElmnt){
	switch (UIElmnt){
		case 0:
			//show gridbox, hide editor
			// ######## gridbox UI
			$('dgbox1-grid-modf-top').setStyle('display', 'block');	
			$('dgbox1-grid-main').setStyle('display', 'block');
			$('dgbox1-grid-modf-bottom').setStyle('display', 'block');
			// ######## editor UI
			$('dgbox1-editor-head').setStyle('display', 'none');		
			$('dgbox1-editor-main').setStyle('display', 'none');	
			$('dgbox1-editor-modf-bottom').setStyle('display', 'none');
			// ######## editor UI field
			
			break;
		case 1:
        	//hide gridbox, show editor add
			// ######## gridbox UI
			$('dgbox1-grid-modf-top').setStyle('display', 'none');	
			$('dgbox1-grid-main').setStyle('display', 'none');
			$('dgbox1-grid-modf-bottom').setStyle('display', 'none');
			// ######## editor UI
			$('rgn-dgbox1-editor-title').set('text', 'New '+dgbox1_Title);
			$('dgbox1-editor-head').setStyle('display', 'block');	
			$('dgbox1-editor-modf-top-1').setStyle('visibility', 'hidden');
			$('dgbox1-editor-modf-top-2').setStyle('visibility', 'hidden');			
			$('dgbox1-editor-main').setStyle('display', 'block');	
			$('dgbox1-editor-modf-bottom').setStyle('display', 'block');	
			// ######## editor UI field, add

			dgbox1_CurrentRecNum = 0;
			// #region Codegen : JS#04
			$('inp-module_uid').set('value','');
			$('inp-module_id').set('value','');
			$('inp-module_name').set('value','');
			$('inp-sort_order').set('value','');
			$('inp-isactive').set('value','True');
			$('inp-description').set('value','');
			// #endregion Codegen : JS#04
			$('inp-create_byfn').set('text','');
			$('inp-create_at').set('text','');
			$('inp-update_byfn').set('text','');
			$('inp-update_at').set('text','');
			setTimeout("bodyscroll.toTop()", 255);
			break;
		case 2:
			//hide gridbox, show editor edit
			// ######## gridbox UI
			$('dgbox1-grid-modf-top').setStyle('display', 'none');	
			$('dgbox1-grid-main').setStyle('display', 'none');
			$('dgbox1-grid-modf-bottom').setStyle('display', 'none');
			// ######## editor UI
			$('rgn-dgbox1-editor-title').set('text', 'Edit '+dgbox1_Title);
			$('dgbox1-editor-head').setStyle('display', 'block');	
		
			$('dgbox1-editor-modf-top-1').setStyle('visibility', 'visible');
			$('dgbox1-editor-modf-top-2').setStyle('visibility', 'visible');	
			$('dgbox1-editor-main').setStyle('display', 'block');	
			//$('dgbox1-editor-modf-bottom').setStyle('display', 'block');			
			// ######## editor UI field, edit
			
			
			//$('inp-DATE_HOLIDAY').focus(); // Codegen JS#00 Manually
			var cellById = dg1.cellById($('inp-module_uid').get('value'), 1).getValue();
			dgbox1_CurrentRecNum = cellById.replace("<b>","").replace("</b>","");  // Codegen JS#00 Manually	
			
			setTimeout("bodyscroll.toTop()", 255);
			break;
	}
    dgbox1_UiState = UIElmnt;
} 	
	

