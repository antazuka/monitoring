﻿<row id="#user_id#">

<userdata name="user_id"><![CDATA[#ud_user_id#]]></userdata>
<userdata name="user_name"><![CDATA[#ud_user_name#]]></userdata>
<userdata name="password"><![CDATA[#ud_password#]]></userdata>
<userdata name="user_type"><![CDATA[#ud_user_type#]]></userdata>
<userdata name="email"><![CDATA[#ud_email#]]></userdata>
<userdata name="phone"><![CDATA[#ud_phone#]]></userdata>
<userdata name="isactive"><![CDATA[#ud_isactive#]]></userdata>

<userdata name="create_by"><![CDATA[#ud_create_by#]]></userdata>
<userdata name="create_byfn"><![CDATA[#ud_create_byfn#]]></userdata>
<userdata name="create_at"><![CDATA[#ud_create_at#]]></userdata>
<userdata name="update_by"><![CDATA[#ud_update_by#]]></userdata>
<userdata name="update_byfn"><![CDATA[#ud_update_byfn#]]></userdata>
<userdata name="update_at"><![CDATA[#ud_update_at#]]></userdata>
<cell>0</cell>
<cell><![CDATA[#recnum#]]></cell>
<cell><![CDATA[#user_id#]]></cell>
<cell><![CDATA[#user_name#]]></cell>
<cell><![CDATA[#user_type_var#]]></cell>
<cell><![CDATA[#email#]]></cell>
<cell><![CDATA[#phone#]]></cell>
<cell><![CDATA[#isactive#]]></cell>


<cell><![CDATA[<a id="dg1-row-#recnum#"></a><div style="font-size:7pt;color:gray;">Posted by #create_byfn#, #create_at# <br>Modified by #update_byfn#, #update_at#</div>]]></cell>
</row>
