
var dg1;
var dg1_Xml;
var dgbox1_Title = ' Role Module ';
var dgbox1_RowsPerPage = 15;
var dgbox1_PageNum = 1;
var dgbox1_PageNumMax = 1;
var dgbox1_MaxRow = 0;
var dgbox1_NoRow = false;
var dgbox1_UiState; //1=insert; 2=update
var dgbox1_AddedRow = 0;
var dgbox1_AddSender = 0; //1=top; 2=bottom
var dgbox1_CurrentRecNum = -1;
var dgbox1_CurrentNav = 0;
var dgbox1_SearchType = 1; // 1=normal search, 2=advanced
var ce1;
var ce2;
var bodyscroll;

var dg2;
var dg2_Xml;
var dgbox2_Title = ' Role Module ';
var dgbox2_RowsPerPage = 15;
var dgbox2_PageNum = 1;
var dgbox2_PageNumMax = 1;
var dgbox2_MaxRow = 0;
var dgbox2_NoRow = false;
var dgbox2_UiState; //1=insert; 2=update
var dgbox2_AddedRow = 0;
var dgbox2_AddSender = 0; //1=top; 2=bottom
var dgbox2_CurrentRecNum = -1;
var dgbox2_CurrentNav = 0;
var dgbox2_SearchType = 1; // 1=normal search, 2=advanced


bodyscroll = new Fx.Scroll(document.body, {
		wait: false,
		duration: 500,
		offset: {'x': 0, 'y': 0},
		transition: Fx.Transitions.Quart.easeOut 
	});	
document.title = 'AGTS - '+ dgbox1_Title;
InitialComponent();


function InitialComponent()
{
	$('bt-search-go').addEvent('click', function(e){	
		dg1__SwithcUIModf(0);
		dg1__Refresh(1);
	});
	$('bt-reset-go').addEvent('click', function(e){	
		$('inp-dgbox1-normal-search-q').set('value','');
		dg1__SwithcUIModf(0);
		dg1__Refresh(1);
	});
	
	
	$('bt-dgbox1-refresh-1').addEvent('click', function(e){	
		dg1__Refresh(dgbox1_PageNum);
	});
	
	$('bt-dgbox1-grid-select-1a').addEvent('click', function(e,c){
		if($defined(dg1)){
			if(this.checked == true) dg1.setCheckedRows(0,1);
			else dg1.setCheckedRows(0,0);
		}
            	
	});	
	
	// button add
	$('bt-dgbox1-add-1').addEvent('click', function(e){	
        $('inp-role_uid').set('value','');
		dg1__SwithcUIModf(1);
	});
	
	$('bt-dgbox1-editor-save-1').addEvent('click', function(e){ 

		if(dgbox1_UiState == 1) { dg1__Save(); }
		else if(dgbox1_UiState == 2) { dg1__Update(); }
			
	});
	
	//multi
	$('bt-dgbox1-delete-multi-1').addEvent('click', function(e){
		var CheckedRows = dg1.getCheckedRows(0);
		if($chk(CheckedRows)){
			if(confirm('Are you sure to delete selected row(s)?')){
				dg1__Delete(String(CheckedRows));
			};
		}
	});
		
	$('bt-dgbox1-editor-delete-single-1').addEvent('click', function(e){	
		var CheckedRows = $('inp-role_uid').get('value');   // Codegen JS#00 Manually
		if($chk(CheckedRows)){
			if(confirm('Are you sure to delete current data?')){
				try {
					dg1__SwithcUIModf(0);						
				} finally {
					dg1__Delete(CheckedRows);
				}										
			};
		}
	});
	
	//back to grid
	$('bt-dgbox1-editor-cancel-1').addEvent('click', function(e){ 
			dg1__SwithcUIModf(0);
			if(dgbox1_CurrentRecNum == 0){
				if(dgbox1_AddSender==1){
					setTimeout("bodyscroll.toTop()", 255);
				} else if(dgbox1_AddSender==2){
					setTimeout("bodyscroll.toBottom()", 255);
				} 
			} else {
				setTimeout("bodyscroll.start(1,$('dg1-row-'+dgbox1_CurrentRecNum).getPosition().y)", 255);			
			}
	});
	
	//paging nav
	$('bt-dgbox1-nav-first-1').addEvent('click', function(e){ 
		dg1__Refresh(1);
	});
	$('bt-dgbox1-nav-first-2').cloneEvents($('bt-dgbox1-nav-first-1'));
	$('bt-dgbox1-nav-prev-1').addEvent('click', function(e){ 
		dg1__Refresh(dgbox1_PageNum-1);
	});
	$('bt-dgbox1-nav-prev-2').cloneEvents($('bt-dgbox1-nav-prev-1'));
	$('bt-dgbox1-nav-next-1').addEvent('click', function(e){ 
		dg1__Refresh(dgbox1_PageNum+1);
	});
	$('bt-dgbox1-nav-next-2').cloneEvents($('bt-dgbox1-nav-next-1'));
	$('bt-dgbox1-nav-last-1').addEvent('click', function(e){ 
		dg1__Refresh(dgbox1_PageNumMax);
	});
	$('bt-dgbox1-nav-last-2').cloneEvents($('bt-dgbox1-nav-last-1'));
	
	if(gup( 'popup' ) == '1'){
		$('bt-dgbox2-popup').setStyle('display','none');
	}
	new FloatingTips('#fffx input', { 
		className: 'global-floating-tip',
		center: false,
		content: 'rel',
		position: 'bottom',
		showOn: 'focus',
		hideOn: 'blur',
		distance: 6
	});				
	
	gTips_Popup = new FloatingTips('#fffy img', { 
		className: 'global-floating-tip',
		center: false,
		content: 'rel',
		position: 'left',
		distance: 6
	});		


	InitialGrid();

}


function InitialGrid()
{
	
	try {
		dg1 = new dhtmlXGridObject('dgbox1');
		dg1.setImagePath('images/dhtmlxGrid/');
		// #region Codegen : JS#00 Manually
		dg1.setHeader(',No,Role ID,Role Name,Is Active,Description,,Date of Record');
		dg1.setInitWidths('20,35,150,200,150,*,80,200');
		dg1.setColAlign('right,left,left,left,left,left,left,left');
		dg1.setColVAlign("middle,middle,middle,middle,middle,middle,middle,middle");
		dg1.setColTypes('ch,ro,ro,ro,ro,ro,ro,ro');
		dg1.setColSorting('int,na,int,str,str,str,na,na');
		dg1.enableResizing('false,true,true,true,true,true,true,true');
		dg1.enableTooltips('false,false,false,false,false,false,false,false');
		// #endregion Codegen : JS#00 Manually
		dg1.enableAutoHeight(true);
		dg1.enableMultiline(true);
		dg1.enableColSpan(true);
		dg1.setSkin('light'); 
		dg1.attachEvent('onRowSelect', function doOnRowSelected(id,ind){
			if(id!='*' && ind!=0 && ind!=1 && ind!=6 ){
				if(!$chk($('inp-role_uid'))) return; // Codegen JS#00 Manually
				$('inp-role_uid').set('value',id);
				$('inp-role_id').set('value',dg1.getUserData(id,'role_id'));
				$('inp-role_name').set('value',dg1.getUserData(id,'role_name'));
				$('inp-description').set('value',dg1.getUserData(id,'description'));
				$('inp-isactive').set('value',dg1.getUserData(id,'isactive'));
				// #endregion Codegen : JS#01
				$('inp-create_by').set('value',dg1.getUserData(id,'create_by'));
				$('inp-create_byfn').set('text',dg1.getUserData(id,'create_byfn'));
				$('inp-create_at').set('text',dg1.getUserData(id,'create_date'));
				$('inp-create_by').set('value',dg1.getUserData(id,'update_by'));
				$('inp-update_byfn').set('text',dg1.getUserData(id,'update_byfn'));
				$('inp-update_at').set('text',dg1.getUserData(id,'update_date'));
				
				
				dg1__SwithcUIModf(2); 
			}
		});
		dg1.attachEvent('onCheckbox', function doOnCheck(rowId, cellInd, state) {                
			if(dgbox1_NoRow==true) return false;
			 else return true;
		});
		dg1.attachEvent("onRowAdded", function(rId){
			 
		}); 
		dg1.init(); 

		
	}
	finally {
		dg1__Refresh(dgbox1_PageNum);
	}
	
}

function dg1__Refresh(pagenumber)
{
	
	var myRequest = new Request({
		url: roles_getByPage,
		data : {
			_token : $('_token').get('value'),
			rowsperpage : dgbox1_RowsPerPage,
			pagenum : pagenumber,
			search : $('inp-dgbox1-normal-search-q').get('value')
		},
		method: 'get',
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none')
				$('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				dgbox1_SuspendRefresh=false;
				dgbox1_MaxRow = response.maxrow;
				dgbox1_NoRow = response.norow;
				dgbox1_PageNum = response.pagenum;
				if(dgbox1_NoRow)dg1.setColumnHidden(0,true);else dg1.setColumnHidden(0,false); 

				var tmpMaxPage = (response.maxrow/dgbox1_RowsPerPage);
				if(tmpMaxPage > tmpMaxPage.round(0)) dgbox1_PageNumMax = tmpMaxPage.round(0) + 1; else dgbox1_PageNumMax = tmpMaxPage.round(0);
				dg1.clearAll();
				dg1.parse(response.data, function(grid_obj,count){
					
					
	                 SetDataGridPagingNavControls(
							dgbox1_MaxRow, dgbox1_PageNum, dgbox1_RowsPerPage, dgbox1_PageNumMax,
							$('bt-dgbox1-nav-current-1'), $('bt-dgbox1-nav-first-1'), $('bt-dgbox1-nav-prev-1'), $('bt-dgbox1-nav-next-1'), $('bt-dgbox1-nav-last-1'),
							$('bt-dgbox1-nav-current-2'), $('bt-dgbox1-nav-first-2'), $('bt-dgbox1-nav-prev-2'), $('bt-dgbox1-nav-next-2'), $('bt-dgbox1-nav-last-2')
					);
					dgbox1_AddedRow = 0;

			     });
				
			}
			
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}


function dg1__Save()
{
	
	
	var myRequest = new Request({
		url: roles_AjaxIns,
		data : {
			_token : $('_token').get('value'),
			role_id : $('inp-role_id').get('value'),
			role_name : $('inp-role_name').get('value'),
			description : $('inp-description').get('value'),
			isactive : $('inp-isactive').get('value'),
			create_by : $('hd-user_id').get('value'),
			create_byfn : $('inp-create_byfn').get('text')
		},
		method: 'post',
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none') $('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				
				if(dgbox1_MaxRow==0){								
					dg1__SwithcUIModf(0); 
					dg1__Refresh(dgbox1_PageNum); 
					Resp = null; 				
				}
				dg1.addRow(response.ud_role_uid,response.data.split('#*#'),0);   
				dg1.setUserData(response.ud_role_uid,'role_uid',response.ud_role_uid);
				dg1.setUserData(response.ud_role_uid,'role_id',response.ud_role_id);
				dg1.setUserData(response.ud_role_uid,'role_name',response.ud_role_name);
				dg1.setUserData(response.ud_role_uid,'description',response.ud_description);
				dg1.setUserData(response.ud_role_uid,'isactive',response.ud_isactive);
				
				dg1.setUserData(response.ud_role_uid,'create_by',response.ud_create_by);
				dg1.setUserData(response.ud_role_uid,'create_byfn',response.ud_create_byfn);
				dg1.setUserData(response.ud_role_uid,'create_at',response.ud_create_at);
				dg1.setUserData(response.ud_role_uid,'update_by',response.ud_update_by);
				dg1.setUserData(response.ud_role_uid,'update_byfn',response.ud_update_byfn);
				dg1.setUserData(response.ud_role_uid,'update_at',response.ud_update_at);
				
				if(dgbox1_PageNumMax == 0) dgbox1_PageNumMax = 1;
				dgbox1_MaxRow = dgbox1_MaxRow+1;
				dgbox1_AddedRow = dgbox1_AddedRow + 1;
				SetDataGridPagingInfoAfterInsert(dgbox1_MaxRow,dgbox1_PageNum,dgbox1_RowsPerPage,dgbox1_PageNumMax,$('bt-dgbox1-nav-current-1'),$('bt-dgbox1-nav-current-2'),dgbox1_AddedRow);
				
				dg1.selectRow(0,false,false,false);
				dg1__SwithcUIModf(0);	
				setTimeout("bodyscroll.toTop()", 255);		
				MessegeInfoShow('Data successfully saved');					
			} 
			else 
			{ alert(response.ErrorMessege); }
			
			
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}

function dg1__Update()
{
	
	var myRequest = new Request({
		url: roles_AjaxUpd,
		data : {
			_token : $('_token').get('value'),
			role_uid : $('inp-role_uid').get('value'),
			role_id : $('inp-role_id').get('value'),
			role_name : $('inp-role_name').get('value'),
			description : $('inp-description').get('value'),
			isactive : $('inp-isactive').get('value'),
			create_by : $('inp-create_by').get('value'),
			create_byfn : $('inp-create_byfn').get('text'),
			craete_at : $('inp-create_at').get('text'),
			update_by : $('hd-user_id').get('value'),
			update_byfn : $('inp-update_byfn').get('text'),
			
			rowidx : dgbox1_CurrentRecNum
		},
		method: 'post',
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none') $('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				
				var idx = dg1.getRowIndex(response.ud_role_uid); 
				if(idx == -1){alert('row not found');return;}
				dg1.deleteRow(response.ud_role_uid);
				dg1.addRow(response.ud_role_uid,response.data.split('#*#'),idx);   
				
				dg1.setUserData(response.ud_role_uid,'role_uid',response.ud_role_uid);
				dg1.setUserData(response.ud_role_uid,'role_id',response.ud_role_id);
				dg1.setUserData(response.ud_role_uid,'role_name',response.ud_role_name);
				dg1.setUserData(response.ud_role_uid,'password',response.ud_password);
				dg1.setUserData(response.ud_role_uid,'role_type',response.ud_role_type);
				dg1.setUserData(response.ud_role_uid,'email',response.ud_email);
				dg1.setUserData(response.ud_role_uid,'phone',response.ud_phone);
				dg1.setUserData(response.ud_role_uid,'isactive',response.ud_isactive);
				
				dg1.setUserData(response.ud_role_uid,'create_by',response.ud_create_by);
				dg1.setUserData(response.ud_role_uid,'create_byfn',response.ud_create_byfn);
				dg1.setUserData(response.ud_role_uid,'create_at',response.ud_create_at);
				dg1.setUserData(response.ud_role_uid,'update_by',response.ud_update_by);
				dg1.setUserData(response.ud_role_uid,'update_byfn',response.ud_update_byfn);
				dg1.setUserData(response.ud_role_uid,'update_at',response.ud_update_at);
				
				dg1.selectRow(idx,false,false,false);
				dg1__SwithcUIModf(0);	
				setTimeout("bodyscroll.toTop()", 255);		
				MessegeInfoShow('Data successfully saved');					
			} 
			else 
			{ alert(response.ErrorMessege); }
			
			
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}

function dg1__Delete(CheckedRows)
{
	var myRequest = new Request({
		url: roles_AjaxDel,
		method: 'delete',
		data : {
			_token : $('_token').get('value'),
			role_uid : CheckedRows
		},
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none') $('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				dg1__Refresh(dgbox1_PageNum);
				setTimeout("bodyscroll.toTop()", 255);
			} 
			else 
			{ alert(response.ErrorMessege); }
			
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}


function dg1__SwithcUIModf(UIElmnt){
	switch (UIElmnt){
		case 0:
			//show gridbox, hide editor
			// ######## gridbox UI
			$('dgbox1-grid-modf-top').setStyle('display', 'block');	
			$('dgbox1-grid-main').setStyle('display', 'block');
			$('dgbox1-grid-modf-bottom').setStyle('display', 'block');
			// ######## editor UI
			$('dgbox1-editor-head').setStyle('display', 'none');		
			$('dgbox1-editor-main').setStyle('display', 'none');	
			$('dgbox1-editor-modf-bottom').setStyle('display', 'none');
			$('top-pan-filter').setStyle('display','block');
			$('top-bottom-pan-filter').setStyle('display','inline');
			// ######## editor UI field
			
			
			break;
		case 1:
        	//hide gridbox, show editor add
			// ######## gridbox UI
			$('dgbox1-grid-modf-top').setStyle('display', 'none');	
			$('dgbox1-grid-main').setStyle('display', 'none');
			$('dgbox1-grid-modf-bottom').setStyle('display', 'none');
			// ######## editor UI
			$('rgn-dgbox1-editor-title').set('text', 'New '+dgbox1_Title);
			$('dgbox1-editor-head').setStyle('display', 'block');	
			$('dgbox1-editor-modf-top-1').setStyle('visibility', 'hidden');
			$('dgbox1-editor-modf-top-2').setStyle('visibility', 'hidden');			
			$('dgbox1-editor-main').setStyle('display', 'block');	
			//$('dgbox1-editor-modf-bottom').setStyle('display', 'block');	
			
			
			$('top-pan-filter').setStyle('display','none');
			$('top-bottom-pan-filter').setStyle('display','none');
			// ######## editor UI field, add
			dgbox1_CurrentRecNum = 0;
			
			// #region Codegen : JS#04
			$('inp-role_id').set('value','');
			$('inp-role_name').set('value','');
			$('inp-description').set('value','');
			$('inp-isactive').set('value',1);
			// #endregion Codegen : JS#04
			$('inp-create_byfn').set('text','');
			$('inp-create_at').set('text','');
			$('inp-update_byfn').set('text','');
			$('inp-update_at').set('text','');
			setTimeout("bodyscroll.toTop()", 255);
			break;
		case 2:
			//hide gridbox, show editor edit
			// ######## gridbox UI
			$('dgbox1-grid-modf-top').setStyle('display', 'none');	
			$('dgbox1-grid-main').setStyle('display', 'none');
			$('dgbox1-grid-modf-bottom').setStyle('display', 'none');
			// ######## editor UI
			$('rgn-dgbox1-editor-title').set('text', 'Edit '+dgbox1_Title);
			$('dgbox1-editor-head').setStyle('display', 'block');	
		
			$('dgbox1-editor-modf-top-1').setStyle('visibility', 'visible');
			$('dgbox1-editor-modf-top-2').setStyle('visibility', 'visible');	
			$('dgbox1-editor-main').setStyle('display', 'block');	
			
			$('top-pan-filter').setStyle('display','none');
			$('top-bottom-pan-filter').setStyle('display','none');
			//$('dgbox1-editor-modf-bottom').setStyle('display', 'block');			
			// ######## editor UI field, edit
						
		
			var cellById = dg1.cellById($('inp-role_uid').get('value'), 1).getValue();
			dgbox1_CurrentRecNum = cellById.replace("<b>","").replace("</b>","");  // Codegen JS#00 Manually	
			
			setTimeout("bodyscroll.toTop()", 255);
			break;
	}
    dgbox1_UiState = UIElmnt;
} 	


var modal;
function viewRolesModule(role_uid,role_name,ui)
{
	
	$('inp-role_uid').set('value',role_uid);
	
	if(modal) modal.destroy();
		modal = new LightFace.Request({
				url: roles_indexSub,
				request: { 
					data: { 
						_token : $('_token').get('value'),
					},
					method: 'get'
				},
				onSuccess: function(response) {
					//alert('tees');
					loadJsRolesSub();
				},
				onFailure: function(response) {
					alert('Cannot load content');
				},
				title: 'Roles Module : ' + role_name, 
				width: 950,
				height: 400,
				draggable: true,
				buttons: [
					{
						title: 'Close',
						event: function() { this.close(); }
					}
				]
		});
		modal.open();	
	
	
	
	
}

function loadJsRolesSub()
{
	InitialComponent2();
}

