
var dg2;
var dg2_Xml;
var dgbox2_Title = ' Role Module ';
var dgbox2_RowsPerPage = 15;
var dgbox2_PageNum = 1;
var dgbox2_PageNumMax = 1;
var dgbox2_MaxRow = 0;
var dgbox2_NoRow = false;
var dgbox2_UiState; //1=insert; 2=update
var dgbox2_AddedRow = 0;
var dgbox2_AddSender = 0; //1=top; 2=bottom
var dgbox2_CurrentRecNum = -1;
var dgbox2_CurrentNav = 0;
var dgbox2_SearchType = 1; // 1=normal search, 2=advanced


function InitialComponent2()
{

	
	//paging nav
	$('bt-dgbox1-nav-first-1').addEvent('click', function(e){ 
		dg1__Refresh(1);
	});
	$('bt-dgbox1-nav-first-2').cloneEvents($('bt-dgbox1-nav-first-1'));
	$('bt-dgbox1-nav-prev-1').addEvent('click', function(e){ 
		dg1__Refresh(dgbox1_PageNum-1);
	});
	$('bt-dgbox1-nav-prev-2').cloneEvents($('bt-dgbox1-nav-prev-1'));
	$('bt-dgbox1-nav-next-1').addEvent('click', function(e){ 
		dg1__Refresh(dgbox1_PageNum+1);
	});
	$('bt-dgbox1-nav-next-2').cloneEvents($('bt-dgbox1-nav-next-1'));
	$('bt-dgbox1-nav-last-1').addEvent('click', function(e){ 
		dg1__Refresh(dgbox1_PageNumMax);
	});
	$('bt-dgbox1-nav-last-2').cloneEvents($('bt-dgbox1-nav-last-1'));
	
	if(gup( 'popup' ) == '1'){
		$('bt-dgbox2-popup').setStyle('display','none');
	}
	
	$('bt-dgbox2-popup').addEvent('click', function(e){ 
		var urlthis=document.URL;
		window.open(urlthis+'&popup=1');
	});
	new FloatingTips('#fffx input', { 
		className: 'global-floating-tip',
		center: false,
		content: 'rel',
		position: 'bottom',
		showOn: 'focus',
		hideOn: 'blur',
		distance: 6
	});				
	
	gTips_Popup = new FloatingTips('#fffy img', { 
		className: 'global-floating-tip',
		center: false,
		content: 'rel',
		position: 'left',
		distance: 6
	});		


	InitialGrid();

}

function InitialGrid()
{
	
	try {
		dg2 = new dhtmlXGridObject('dgbox2');
		dg2.setImagePath('images/dhtmlxGrid/');
		// #region Codegen : JS#00 Manually
		dg2.setHeader(',No,Submodule Name,Addable,Editable,Deleteable,Date of Record');
		dg2.setInitWidths('35,35,*,100,100,100,150');
		dg2.setColAlign('left,right,left,left,left,left,left');
		dg2.setColVAlign("middle,middle,center,middle,middle,middle,middle");
		dg2.setColTypes('ro,ro,ro,ro,ro,ro,ro');
		dg2.setColSorting('int,int,str,int,int,int,na');
		dg2.enableResizing('false,true,true,true,true,true,true');
		dg2.enableTooltips('false,false,false,false,false,false,false');
		// #endregion Codegen : JS#00 Manually
		//gup( 'popup' ) == '1' ? dg1.enableAutoHeight(true) : dg1.enableAutoHeight(false);
		dg2.enableMultiline(true);
		dg2.enableColSpan(true);
		dg2.setSkin('light'); 
		dg2.attachEvent('onRowSelect', function doOnRowSelected(id,ind){
			
			if(id!='*' && ind!=0 && ind!=1 && ind!=6 ){
				
			}
		});
		dg2.attachEvent('onCheckbox', function doOnCheck(rowId, cellInd, state) {                
			if(dgbox2_NoRow==true) return false;
			 else return true;
		});
		dg2.attachEvent("onRowAdded", function(rId){
			 
		}); 
		dg2.setColumnHidden(6,true)
		dg2.init(); 	
		
		
	}
	finally {
		dg2__Refresh(dgbox1_PageNum);
	}
	
}

function dg2__Refresh(pagenumber)
{
	
	var myRequest = new Request({
		url: roles_getByPageMod,
		data : {
			_token : $('_token').get('value'),
			rowsperpage : dgbox2_RowsPerPage,
			pagenum : 1,
			search : $('inp-dgbox1-normal-search-q').get('value'),
			role_uid : $('inp-role_uid').get('value')
		},
		method: 'get',
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none')
				$('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				//dgbox2_SuspendRefresh=false;
				
				dgbox2_MaxRow = response.maxrow;
				dgbox2_NoRow = response.norow;
				dgbox2_PageNum = response.pagenum;
				if(dgbox2_NoRow)dg2.setColumnHidden(0,true);else dg2.setColumnHidden(0,false); 

				var tmpMaxPage = (response.maxrow/dgbox2_RowsPerPage);
				if(tmpMaxPage > tmpMaxPage.round(0)) dgbox2_PageNumMax = tmpMaxPage.round(0) + 1; else dgbox2_PageNumMax = tmpMaxPage.round(0);
				dg2.clearAll();
				dg2.parse(response.data, function(grid_obj,count){
					
					
	                 SetDataGridPagingNavControls(
							dgbox2_MaxRow, dgbox2_PageNum, dgbox2_RowsPerPage, dgbox2_PageNumMax,
							$('bt-dgbox2-nav-current-1'), $('bt-dgbox2-nav-first-1'), $('bt-dgbox2-nav-prev-1'), $('bt-dgbox2-nav-next-1'), $('bt-dgbox2-nav-last-1'),
							$('bt-dgbox2-nav-current-2'), $('bt-dgbox2-nav-first-2'), $('bt-dgbox2-nav-prev-2'), $('bt-dgbox2-nav-next-2'), $('bt-dgbox2-nav-last-2')
					);
					dgbox2_AddedRow = 0;

			     });
				
			}
			
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
	
}

function SubmitRolesSubmodule(role_uid,submodule_uid,fields)
{
	var rowid=submodule_uid;
	var ISSELECTED = dg2.getUserData(rowid,'isselected');
	var ADDABLE = dg2.getUserData(rowid,'addable');
	var EDITABLE = dg2.getUserData(rowid,'editable');
	var DELETEABLE = dg2.getUserData(rowid,'deleteable');
	
	var valueSelected = 0
	
	if(fields == "1"){
		if(ADDABLE == 0){ valueSelected = 1; }else { valueSelected = 0;}
	}else if(fields == "2"){
		if(EDITABLE == 0){ valueSelected = 1; }else { valueSelected = 0;}
	}else if (fields ==  "3"){
		if(DELETEABLE == 0){ valueSelected = 1; }else { valueSelected = 0;}
	}
		
	if(fields == '0')
	{
		if(ISSELECTED==0){
			AddRolesSubmodule(role_uid,submodule_uid);
		} else {
			RemoveRolesSubmodule(role_uid,submodule_uid);
		}	
	}else {
		UpdateRolesSubmodule(role_uid,submodule_uid,fields,valueSelected);
	}
	
	
	
}

function AddRolesSubmodule(role_uid,submodule_uid){
	
	var myRequest = new Request({
		url: roles_AjaxInsChld,
		data : {
			_token : $('_token').get('value'),
			role_uid : role_uid,
			submodule_uid : submodule_uid,
			create_by : $('hd-user_id').get('value'),
			update_byfn : $('inp-update_byfn').get('text')
		},
		method: 'post',
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none')
				$('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				dg2__Refresh(1);
			}
			
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}
function RemoveRolesSubmodule(role_uid,submodule_uid){
	
	var myRequest = new Request({
		url: roles_AjaxDelChld,
		data : {
			_token : $('_token').get('value'),
			role_uid : role_uid,
			submodule_uid : submodule_uid
		},
		method: 'delete',
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none')
				$('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				dg2__Refresh(1);
			}
			
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}

function UpdateRolesSubmodule(role_uid,submodule_uid,fields,valueselected){
	
	var myRequest = new Request({
		url: roles_AjaxUpdChld,
		data : {
			_token : $('_token').get('value'),
			role_uid : role_uid,
			submodule_uid : submodule_uid,
			fields : fields,
			valueselected : valueselected,
			update_by : $('hd-user_id').get('value'),
			update_byfn : $('inp-update_byfn').get('text')
		},
		method: 'post',
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none')
				$('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				dg2__Refresh(1);
			}
			
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 	
}
