
@extends('layout.main_grid')

@section('content')
		
		<input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
		<div id="hde-messege-info" style="display:none;"> 				
		</div>  
		<div id="rgn-MODAL_CONTENT"></div>
		<div style="clear:both; width:100%; text-align:left; vertical-align:middle; padding:5px 5px 5px 5px; ">		
			<div style="clear:right; float:left; padding-right:15px;" class="ModuleTitle" >
				Roles Module
			</div>	
			<div id="top-pan-filter">
				<table style="font-size:12px;padding-top:8px;float:left">	
						<tr>
							
							<td>
								<input id="inp-dgbox1-normal-search-q" class="text-325 relative-left" maxLength="60"  type="text"></input>
							</td>
							<td>
								<a id="bt-search-go" class="button" style="font-size:10px;"><span>Search</span></a>
							</td>
							<td>
								<a id="bt-reset-go" class="button" style="font-size:10px;"><span>Reset</span></a>
							</td>
						</tr>
					</table>
					
					<div style="clear:right; float:right; padding-right:15px;" >
						
					</div>		
			</div>	
					
		</div>

		<hr class="style13">

		<div id="top-bottom-pan-filter" style="float:left; display:inline;">	
			<div  style="clear:left; float:left; padding:0px 0px 0px 10px;" >
				<!--
				<table cellspacing="0" cellpadding="2" border="0" >
									
								  <tr>
									<td style="width:10px">&nbsp;</td>
									<td>
										<label for="inp-module_id">
											<div><b>Module ID :</b></div>
											<input type="text" id="inp-module_id" class="text-325">
										</label>
									</td>
								  </tr>   
				</table
				-->
				
			</div>
	
		</div>	
			
		
	
		<div style="clear:both; text-align:center; width:100%; padding-top:5px;" >
			<!-- #CUSTOMERTYPE: BODY-CONTENT CONTAINER -->
			<div style="clear:both;margin-bottom:10px;">
				<div class="br-foot br-barbg br-shadow" style="height:24px;padding:4px;"> <!-- refresh, add, delete  -->
					<div id="dgbox1-grid-modf-top" style="clear:none;">
						<div class="relative-left" >
							<img src="{{asset('images/icons/modf-wh-t.png')}}" class="relative-left" style="clear: left ! important; margin-left: 5px; margin-right: 5px;margin-top: 4px;">						
							<a id="bt-dgbox1-grid-select-1" ><span style="padding:10px 3px 0px 0px;"><input id="bt-dgbox1-grid-select-1a" type="checkbox"></span></a>
							<a id="bt-dgbox1-refresh-1" class="button" ><img class="button-ico" src="images/icons/refresh-sm.png"><span>&nbsp;&nbsp;Refresh&nbsp;&nbsp;&nbsp;&nbsp;</span></a>
							<a id="bt-dgbox1-add-1" class="button"><img class="button-ico" src="images/icons/add.png"><span>&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;&nbsp;</span></a>	
							<a id="bt-dgbox1-delete-multi-1" class="button"><img class="button-ico" src="images/icons/trash.png"><span>&nbsp;&nbsp;Delete&nbsp;&nbsp;&nbsp;&nbsp;</span></a>&nbsp;&nbsp;
							<a id="bt-dgbox1-grid-export-1" class="button-icon icon chat" style="display:none;"><span>Export</span></a>
						</div>				
						<div class="relative-right" style="font-family:Arial;font-size:12px;padding:3px 10px 0px 0px;">
							
							<span id="bt-dgbox1-nav-first-1" class="HLink" style="color:blue; display:inline;"><img src="images/icons/rewind.png"></span>&nbsp;   <!-- first -->
							<span id="bt-dgbox1-nav-prev-1" class="HLink" style="color:blue; display:inline;"><img src="images/icons/skip_backward.png"></span>&nbsp; <!-- Previous -->
							<span id="bt-dgbox1-nav-current-1"style=" display:inline;vertical-align:top;"></span>&nbsp;
							<span id="bt-dgbox1-nav-next-1" class="HLink" style="color:blue; display:inline;"><img src="images/icons/skip_forward.png"></span>&nbsp; <!-- next -->
							<span id="bt-dgbox1-nav-last-1" class="HLink" style="color:blue; display:inline;" ><img src="images/icons/fast_forward.png"></span>  <!-- last -->
						</div> 
					
					</div>
					<div id="dgbox1-editor-head" style="clear:none;display:none;">
						<img src="images/icons/link_item-wh.png" class="relative-left" style="float: left ! important; margin-left: 5px; margin-right: 5px; margin-top: 2px;">	
						<div id="rgn-dgbox1-editor-title" class="br-head-title relative-left">Title Goes Here</div>	
						<div class="relative-left left-pad-10">
							<a id="bt-dgbox1-editor-cancel-1" class="button"><img class="button-ico" src="images/icons/back.png"><span>&nbsp;&nbsp;Back to Main&nbsp;&nbsp;&nbsp;&nbsp;</span></a>
							<a id="bt-dgbox1-editor-save-1" class="button"><img class="button-ico" src="images/icons/save.png"><span>&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;&nbsp;</span></a>								
						</div>
						<div id="dgbox1-editor-modf-top-1" class="relative-right right-pad-10">
						
							<a id="bt-dgbox1-editor-delete-single-1" class="button"><img class="button-ico" src="images/icons/trash.png"><span>&nbsp;&nbsp;Delete&nbsp;&nbsp;</span></a>
						</div>
					</div>
				</div>	
				
				<div id="dgbox1-grid-main" class="br-mid br-shadow" > <!-- dgbox1 grid -->
					<div id="dgbox1-filter-state" style="padding: 5px 20px; font-size: 10px;display:none;">
						<span class="filter-label">displaying data filter</span>
					</div>
					<div id="dgbox1" style="width:auto;clear:both;height:10px">
					</div>						
				</div>
				<div id="dgbox1-editor-main" class="br-mid br-shadow" style="margin-top:0px;display:none;">  <!-- dgbox1 editor-->				
					<div class="br-content">
									
						<div class="br-full br-shadow" style="margin-top:5px;margin-bottom:5px;">
						
														
							<div id="dgbox1-editor-tab1-content" style="clear:both;" class="br-content">
								<br/>
								<input  type="hidden"  id="inp-role_uid">
								
								<table cellspacing="0" cellpadding="2" border="0" style="color:#055A78;font-size:12px;">
									<tr valign="top">
										<td>
											
											<table  style="color:#055A78;font-size:12px;">
												<tr>
													<td>  <b>Role ID </b> </td>
													<td>:</td>
													<td>
														<input type="text" id="inp-role_id" class="text-325">
													</td>
												</tr>
												<tr>
													<td> <b>Role Name </b> </td>
													<td>:</td>
													<td>
														<input type="text" id="inp-role_name" class="text-325">
													</td>
												</tr>
												<tr>
													<td> <b>Is Active </b> </td>
													<td>:</td>
													<td>
														<select id="inp-isactive">
															<option value=1>Aktif</option>
															<option value=2>Tidak Aktif</option> 
														</select>
													</td>
												</tr>
												<tr>
													<td> <b>Description </b> </td>
													<td>:</td>
													<td>
														<textarea id="inp-description" name="inp-rgn-description" cols="100" rows="4" style="font: 12px Arial;"></textarea>
													</td>
												</tr>
												
												
											</table>
										
										
										
										</td>
										<td>
										
											<table  style="color:#055A78;font-size:12px;">
												<tr>
												</tr>
												
											</table>
												
										</td>
									</tr>
								</table>
								<br/><br/>
								
								
								<table cellspacing="0" cellpadding="2" border="0" >
								  <tr>
									<td style="width:10px">&nbsp;</td>
									<td>
										<input type="hidden" id="inp-create_by"><input type="hidden" id="inp-update_by">
										
									</td>
								  </tr>
								</table>													

							</div>
							
							
											
							
							<div class="br-modf-foot" >
								<div id="rgn-dgbox1-modf-foot">
									Posted by : <span id="inp-create_byfn" ></span> in <span id="inp-create_at" ></span>&nbsp;&nbsp;&nbsp;~&nbsp;&nbsp;&nbsp;Modifed by : <span id="inp-update_byfn" ></span> in <span id="inp-update_at" ></span>
								</div>								
							</div>
						</div>
									
					</div>
				</div>						
				
				<div id="dgbox1-editor-modf-bottom" class="br-foot br-barbg br-shadow"  style="padding:4px;display:none;"><!-- Cancel, save, delete  -->
					<div class="relative-left left-pad-10">	
					
						<a id="bt-dgbox1-editor-cancel-2" class="button button-blue"><span>&nbsp;&lsaquo; Back to Grid&nbsp;</span></a>
						<a id="bt-dgbox1-editor-save-2" class="button button-blue"><span>&nbsp;&nbsp;Save&nbsp;&nbsp;</span></a> 							
					</div>
					<div id="dgbox1-editor-modf-top-2" class="relative-right right-pad-10">
		
						<a id="bt-dgbox1-editor-delete-single-2" class="button"><img class="button-ico" src="images/icons/trash.png"><span>&nbsp;&nbsp;Delete&nbsp;&nbsp;</span></a> 
					</div>
				</div>
				<div id="dgbox1-grid-modf-bottom" class="br-botm br-shadow" style="margin-top:0px;"> <!-- refresh, add, delete  -->
					<div style="clear:both;border-top:1px solid #EAEAEA;height:23px;padding:4px;">
						<div class="relative-left">
							
						</div>
						<div class="relative-right" style="font-family:Arial;font-size:12px; padding:3px 10px 0px 0px;">
						
							<span id="bt-dgbox1-nav-first-2" class="HLink" style="color:blue; display:inline;"><img src="images/icons/rewind.png"></span>&nbsp;   <!-- first -->
							<span id="bt-dgbox1-nav-prev-2" class="HLink" style="color:blue; display:inline;"><img src="images/icons/skip_backward.png"></span>&nbsp; <!-- Previous -->
							<span id="bt-dgbox1-nav-current-2"style=" display:inline;vertical-align:top;"></span>&nbsp;
							<span id="bt-dgbox1-nav-next-2" class="HLink" style="color:blue; display:inline;"><img src="images/icons/skip_forward.png"></span>&nbsp; <!-- next -->
							<span id="bt-dgbox1-nav-last-2" class="HLink" style="color:blue; display:inline;" ><img src="images/icons/fast_forward.png"></span>  <!-- last -->
							
						</div>								
					</div>					
				</div>
				
				
				
			</div>
	        <!-- #ENDCUSTOMERTYPE: BODY-CONTENT CONTAINER -->  
		</div>
				
		<script>
			
			// global app configuration object
			var roles_getByPage= "{{ URL::to('roles_getByPage') }}" ;
			var roles_AjaxIns= "{{ URL::to('roles_ins') }}" ;
			var roles_AjaxUpd= "{{ URL::to('roles_upd') }}" ;
			var roles_AjaxDel= "{{ URL::to('roles_del') }}" ;
			var roles_indexSub= "{{ URL::to('roles_indexSub') }}" ;
			var roles_getByPageMod= "{{ URL::to('roles_getByPageMod') }}" ;
			var roles_AjaxInsChld= "{{ URL::to('roles_AjaxInsChld') }}" ;
			var roles_AjaxDelChld= "{{ URL::to('roles_AjaxDelChld') }}" ;
			var roles_AjaxUpdChld= "{{ URL::to('roles_AjaxUpdChld') }}" ;
		</script>

		<script src="{{ url('../resources/views/module/roles/roles.js') }}" type="text/javascript"></script>
		<script src="{{ url('../resources/views/module/roles/rolessub.js') }}" type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" media="all" href="{{url('../resources/views/module/roles/roles.css')}}" />
		
	
		
		
@stop()

