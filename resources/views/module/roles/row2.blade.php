﻿<row id="#submodule_uid#">

<userdata name="role_uid"><![CDATA[#ud_role_uid#]]></userdata>
<userdata name="submodule_uid"><![CDATA[#ud_submodule_uid#]]></userdata>
<userdata name="submodule_id"><![CDATA[#ud_submodule_id#]]></userdata>
<userdata name="submodule_name"><![CDATA[#ud_submodule_name#]]></userdata>
<userdata name="isselected"><![CDATA[#ud_isselected#]]></userdata>
<userdata name="editable"><![CDATA[#ud_editable#]]></userdata>
<userdata name="addable"><![CDATA[#ud_addable#]]></userdata>
<userdata name="deleteable"><![CDATA[#ud_deleteable#]]></userdata>
<userdata name="description"><![CDATA[#ud_description#]]></userdata>

<userdata name="create_by"><![CDATA[#ud_create_by#]]></userdata>
<userdata name="create_byfn"><![CDATA[#ud_create_byfn#]]></userdata>
<userdata name="create_at"><![CDATA[#ud_create_at#]]></userdata>
<userdata name="update_by"><![CDATA[#ud_update_by#]]></userdata>
<userdata name="update_byfn"><![CDATA[#ud_update_byfn#]]></userdata>
<userdata name="update_at"><![CDATA[#ud_update_at#]]></userdata>

<cell><![CDATA[#isselected#]]></cell>
<cell><![CDATA[#recnum#]]></cell>
<cell><![CDATA[
	<div style="height:20px">#submodule_name#</div>
]]></cell>

<cell><![CDATA[#addable#]]></cell>
<cell><![CDATA[#editable#]]></cell>
<cell><![CDATA[#deleteable#]]></cell>

<cell><![CDATA[<a id="dg2-row-#recnum#"></a><div style="font-size:7pt;color:gray;">Modified by #update_byfn#, #update_at#</div>]]></cell>
</row>
