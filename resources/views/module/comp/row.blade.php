﻿<row id="#comp_uid#">

<userdata name="comp_uid"><![CDATA[#ud_comp_uid#]]></userdata>
<userdata name="comp_id"><![CDATA[#ud_comp_id#]]></userdata>
<userdata name="comp_name"><![CDATA[#ud_comp_id#]]></userdata>
<userdata name="comp_address"><![CDATA[#ud_comp_address#]]></userdata>
<userdata name="comp_propinsi"><![CDATA[#ud_comp_propinsi#]]></userdata>
<userdata name="comp_kabupaten"><![CDATA[#ud_comp_kabupaten#]]></userdata>
<userdata name="comp_kecamatan"><![CDATA[#ud_comp_kecamatan#]]></userdata>
<userdata name="comp_kelurahan"><![CDATA[#ud_comp_kelurahan#]]></userdata>
<userdata name="comp_email"><![CDATA[#ud_comp_email#]]></userdata>
<userdata name="comp_telp"><![CDATA[#ud_comp_telp#]]></userdata>
<userdata name="isactive"><![CDATA[#ud_isactive#]]></userdata>

<userdata name="create_by"><![CDATA[#ud_create_by#]]></userdata>
<userdata name="create_byfn"><![CDATA[#ud_create_byfn#]]></userdata>
<userdata name="create_at"><![CDATA[#ud_create_at#]]></userdata>
<userdata name="update_by"><![CDATA[#ud_update_by#]]></userdata>
<userdata name="update_byfn"><![CDATA[#ud_update_byfn#]]></userdata>
<userdata name="update_at"><![CDATA[#ud_update_at#]]></userdata>
<cell>0</cell>
<cell><![CDATA[#recnum#]]></cell>
<cell><![CDATA[#comp_id#]]></cell>
<cell><![CDATA[#comp_name#]]></cell>
<cell><![CDATA[#comp_address#]]></cell>
<cell><![CDATA[#comp_email#]]></cell>
<cell><![CDATA[#comp_telp#]]></cell>
<cell><![CDATA[#isactive#]]></cell>

<cell><![CDATA[<a id="dg1-row-#recnum#"></a><div style="font-size:7pt;color:gray;">Create by #create_byfn#, #create_at# <br>Update by #update_byfn#, #update_at#</div>]]></cell>
</row>
