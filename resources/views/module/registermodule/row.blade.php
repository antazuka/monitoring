﻿<row id="#module_uid#">

<userdata name="module_uid"><![CDATA[#ud_module_uid#]]></userdata>
<userdata name="module_id"><![CDATA[#ud_module_id#]]></userdata>
<userdata name="module_name"><![CDATA[#ud_module_name#]]></userdata>
<userdata name="isactive"><![CDATA[#ud_isactive#]]></userdata>
<userdata name="description"><![CDATA[#ud_description#]]></userdata>
<userdata name="sort_order"><![CDATA[#ud_sort_order#]]></userdata>

<userdata name="create_by"><![CDATA[#ud_create_by#]]></userdata>
<userdata name="create_byfn"><![CDATA[#ud_create_byfn#]]></userdata>
<userdata name="create_at"><![CDATA[#ud_create_at#]]></userdata>
<userdata name="update_by"><![CDATA[#ud_update_by#]]></userdata>
<userdata name="update_byfn"><![CDATA[#ud_update_byfn#]]></userdata>
<userdata name="update_at"><![CDATA[#ud_update_at#]]></userdata>
<cell>0</cell>
<cell><![CDATA[<a class="button" style="margin-top:2px;margin-bottom:2px;font-size:10px;" onclick="viewDetail('#module_uid#','#module_id#','#module_name#')"><span>Detail</span></a>]]></cell>
<cell><![CDATA[#recnum#]]></cell>
<cell><![CDATA[#module_id#]]></cell>
<cell><![CDATA[#module_name#]]></cell>
<cell><![CDATA[#isactive#]]></cell>
<cell><![CDATA[#sort_order#]]></cell>
<cell><![CDATA[#description#]]></cell>

<cell><![CDATA[<a id="dg1-row-#recnum#"></a><div style="font-size:7pt;color:gray;">Posted by #create_byfn#, #create_at# <br>Modified by #update_byfn#, #update_at#</div>]]></cell>
</row>
