
var dg1;
var dg1_Xml;
var dgbox1_Title = ' Module ';
var dgbox1_RowsPerPage = 15;
var dgbox1_PageNum = 1;
var dgbox1_PageNumMax = 1;
var dgbox1_MaxRow = 0;
var dgbox1_NoRow = false;
var dgbox1_UiState; //1=insert; 2=update
var dgbox1_AddedRow = 0;
var dgbox1_AddSender = 0; //1=top; 2=bottom
var dgbox1_CurrentRecNum = -1;
var dgbox1_CurrentNav = 0;
var dgbox1_SearchType = 1; // 1=normal search, 2=advanced
var ce1;
var ce2;
var bodyscroll;
var gTips;

var dgbox1_editor_activetabs = -1; // 1=tab1, 2=tab2 dst

var dg2;
var dg2_Xml;
var dgbox2_Title = 'Sub Module';
var dgbox2_RowsPerPage = 15;
var dgbox2_PageNum = 1;
var dgbox2_PageNumMax = 1;
var dgbox2_MaxRow = 0;
var dgbox2_NoRow = false;
var dgbox2_UiState; //1=insert; 2=update
var dgbox2_AddedRow = 0;
var dgbox2_AddSender = 0; //1=top; 2=bottom
var dgbox2_CurrentRecNum = -1;
var dgbox2_SearchType = 1; // 1=normal search, 2=advanced
var dgbox2_Refresh_Trigger=1;

var dgbox1_SuspendRefresh=false;
var expandGrid = 0; // ke expand

bodyscroll = new Fx.Scroll(document.body, {
		wait: false,
		duration: 500,
		offset: {'x': 0, 'y': 0},
		transition: Fx.Transitions.Quart.easeOut 
	});	
document.title = 'AGTS - '+ dgbox1_Title;
InitialComponent();


function InitialComponent()
{
	$('bt-search-go').addEvent('click', function(e){	
		dg1__SwithcUIModf(0);
		dg1__Refresh(1);
	});
	$('bt-reset-go').addEvent('click', function(e){	
		$('inp-dgbox1-normal-search-q').set('value','');
		dg1__SwithcUIModf(0);
		dg1__Refresh(1);
	});
	
	
	$('bt-dgbox1-refresh-1').addEvent('click', function(e){	
		dg1__Refresh(dgbox1_PageNum);
	});
	
	$('bt-dgbox1-grid-select-1a').addEvent('click', function(e,c){
		//var checked = $('bt-dgbox1-grid-select-2a').set('checked',this.checked);
		if($defined(dg1)){
			if(this.checked == true) dg1.setCheckedRows(0,1);
			else dg1.setCheckedRows(0,0);
		}
            	
	});	
	
	// button add
	$('bt-dgbox1-add-1').addEvent('click', function(e){	
        $('inp-module_uid').set('value','');
		dg1__SwithcUIModf(1);
	});
	
	$('bt-dgbox1-editor-save-1').addEvent('click', function(e){ 

		if(dgbox1_UiState == 1) { dg1__Save(); }
		else if(dgbox1_UiState == 2) { dg1__Update(); }
			
	});
	
	//multi
	$('bt-dgbox1-delete-multi-1').addEvent('click', function(e){
		var CheckedRows = dg1.getCheckedRows(0);
		if($chk(CheckedRows)){
			if(confirm('Are you sure to delete selected row(s)?')){
				//alert(CheckedRows);
				dg1__Delete(String(CheckedRows));
			};
		}
	});
		
	$('bt-dgbox1-editor-delete-single-1').addEvent('click', function(e){	
		var CheckedRows = $('inp-module_uid').get('value');   // Codegen JS#00 Manually
		if($chk(CheckedRows)){
			if(confirm('Are you sure to delete current data?')){
				try {
					dg1__SwithcUIModf(0);						
				} finally {
					dg1__Delete(CheckedRows);
				}										
			};
		}
	});
	
	//back to grid
	$('bt-dgbox1-editor-cancel-1').addEvent('click', function(e){ 
			dg1__SwithcUIModf(0);
			if(dgbox1_CurrentRecNum == 0){
				if(dgbox1_AddSender==1){
					setTimeout("bodyscroll.toTop()", 255);
				} else if(dgbox1_AddSender==2){
					setTimeout("bodyscroll.toBottom()", 255);
				} 
			} else {
				setTimeout("bodyscroll.start(1,$('dg1-row-'+dgbox1_CurrentRecNum).getPosition().y)", 255);			
			}
	});
	
	//paging nav
	$('bt-dgbox1-nav-first-1').addEvent('click', function(e){ 
		dg1__Refresh(1);
	});
	$('bt-dgbox1-nav-first-2').cloneEvents($('bt-dgbox1-nav-first-1'));
	$('bt-dgbox1-nav-prev-1').addEvent('click', function(e){ 
		dg1__Refresh(dgbox1_PageNum-1);
	});
	$('bt-dgbox1-nav-prev-2').cloneEvents($('bt-dgbox1-nav-prev-1'));
	$('bt-dgbox1-nav-next-1').addEvent('click', function(e){ 
		dg1__Refresh(dgbox1_PageNum+1);
	});
	$('bt-dgbox1-nav-next-2').cloneEvents($('bt-dgbox1-nav-next-1'));
	$('bt-dgbox1-nav-last-1').addEvent('click', function(e){ 
		dg1__Refresh(dgbox1_PageNumMax);
	});
	$('bt-dgbox1-nav-last-2').cloneEvents($('bt-dgbox1-nav-last-1'));

	// tab selector
	/*
	$('dgbox1-editor-tab1').addEvent('click', function(e){ 
			dg1__SwithcUIModfTab(1);
	});
	$('dgbox1-editor-tab2').addEvent('click', function(e){ 
			dg1__SwithcUIModfTab(2);
	});
	*/
	
	/*   SITE CONTROL GRID 2  */
	
	$('bt-dgbox2-search-go').addEvent('click', function(e){	
		dg2__SwithcUIModf(0);
		dg2__Refresh(1);
	});
	$('bt-dgbox2-reset-go').addEvent('click', function(e){	
		$('inp-dgbox2-normal-search-q').set('value','');
		dg2__SwithcUIModf(0);
		dg2__Refresh(1);
	});
	
	
	//button refresh
	$('bt-dgbox2-refresh-1').addEvent('click', function(e){	
		dg2__Refresh(1);	
	});
	//$('bt-dgbox2-grid-refresh-2').cloneEvents($('bt-dgbox2-grid-refresh-1'));
	
	$('bt-dgbox2-grid-select-1a').addEvent('click', function(e,c){
		//var checked = $('bt-dgbox1-grid-select-2a').set('checked',this.checked);
		if($defined(dg2)){
			if(this.checked == true) dg2.setCheckedRows(0,1);
			else dg2.setCheckedRows(0,0);
		}
            	
	});	
	
	//button Add
	$('bt-dgbox2-add-1').addEvent('click', function(e){	
		$('inp-chd1-submodule_uid').set('value','');
		dg2__SwithcUIModf(1);
	});
	//$('bt-dgbox2-add-2').cloneEvents($('bt-dgbox2-add-1'));
	$('bt-dgbox2-add-1').addEvent('click', function(e){ dgbox2_AddSender = 1; });
	//$('bt-dgbox2-add-2').addEvent('click', function(e){ dgbox2_AddSender = 2; });	
	
	//button save
	$('bt-dgbox2-editor-save-1').addEvent('click', function(e){ 
		if(dgbox2_UiState == 1) { dg2__Save(); }
		else if(dgbox2_UiState == 2) { dg2__Update(); }
	});
	$('bt-dgbox2-editor-save-2').cloneEvents($('bt-dgbox2-editor-save-1'));        
	//button cancel
	$('bt-dgbox2-editor-cancel-1').addEvent('click', function(e){ 
		dg2__SwithcUIModf(0);
		if(dgbox2_CurrentRecNum == 0){
			if(dgbox2_AddSender==1){
				//setTimeout("bodyscroll.start(0,$('bt-dgbox2-editor-cancel-1').getPosition().y)", 255);
				setTimeout("bodyscroll.toTop()", 255);
			} else if(dgbox2_AddSender==2){
				//setTimeout("bodyscroll.start(0,$('bt-dgbox2-editor-cancel-2').getPosition().y)", 255);
				setTimeout("bodyscroll.toTop()", 255);
			} 
		} else {
			setTimeout("bodyscroll.start(0,$('dg2-row-'+dgbox2_CurrentRecNum).getPosition().y)", 255);			
		}
	});
	$('bt-dgbox2-editor-cancel-2').cloneEvents($('bt-dgbox2-editor-cancel-1'));		
	
	
	//button delete single 
	$('bt-dgbox2-editor-delete-single-1').addEvent('click', function(e){
		var submodule_uid = $('inp-chd1-submodule_uid').get('value');   // Codegen JS#00 Manually
		var module_uid = $('inp-module_uid').get('value');
		if($chk(module_uid) && $chk(submodule_uid)){
			if(confirm('Are you sure to delete current data?')){
				try {
					dg2__SwithcUIModf(0);						
				} finally {
					dg2__Delete(submodule_uid,module_uid);
				}										
			};
		}
	});	
	$('bt-dgbox2-editor-delete-single-2').cloneEvents($('bt-dgbox2-editor-delete-single-1')); 
	
	$('bt-dgbox2-delete-multi-1').addEvent('click', function(e){
		var CheckedRows = dg2.getCheckedRows(0);
		if($chk(CheckedRows)){
			if(confirm('Are you sure to delete selected row(s)?')){
				//alert(CheckedRows);
				dg2__Delete(String(CheckedRows));
			};
		}
	});

	//paging nav
	$('bt-dgbox2-nav-first-1').addEvent('click', function(e){ 
		dg2__Refresh(1);
	});
	$('bt-dgbox2-nav-first-2').cloneEvents($('bt-dgbox2-nav-first-1'));
	$('bt-dgbox2-nav-prev-1').addEvent('click', function(e){ 
		dg2__Refresh(dgbox2_PageNum-1);
	});
	$('bt-dgbox2-nav-prev-2').cloneEvents($('bt-dgbox2-nav-prev-1'));
	$('bt-dgbox2-nav-next-1').addEvent('click', function(e){ 
		dg2__Refresh(dgbox2_PageNum+1);
	});
	$('bt-dgbox2-nav-next-2').cloneEvents($('bt-dgbox2-nav-next-1'));
	$('bt-dgbox2-nav-last-1').addEvent('click', function(e){ 
		dg2__Refresh(dgbox2_PageNumMax);
	});
	$('bt-dgbox2-nav-last-2').cloneEvents($('bt-dgbox2-nav-last-1'));
   
		
	
	InitialGrid();

}

function InitialGrid()
{
	
	try {
		dg1 = new dhtmlXGridObject('dgbox1');
		dg1.setImagePath('images/dhtmlxGrid/');
		// #region Codegen : JS#00 Manually
		dg1.setHeader(',,No,Module ID,Module Name,Active,Sort Order,Description,Date of Record');
		dg1.setInitWidths('20,65,35,150,250,150,150,*,200');
		dg1.setColAlign('right,left,center,left,left,left,left,left,left');
		dg1.setColVAlign("middle,middle,middle,middle,middle,middle,middle,middle,middle");
		dg1.setColTypes('ch,ro,ro,ro,ro,ro,ro,ro,ro');
		dg1.setColSorting('int,na,int,str,str,str,str,str,na');
		dg1.enableResizing('false,true,true,true,true,true,true,true,true');
		dg1.enableTooltips('false,false,false,false,false,false,false,false,false');
		// #endregion Codegen : JS#00 Manually
		dg1.enableAutoHeight(true);
		dg1.enableMultiline(true);
		dg1.enableColSpan(true);
		dg1.setSkin('light'); 
		dg1.attachEvent('onRowSelect', function doOnRowSelected(id,ind){
			if(id!='*' && ind!=0 && ind!=1 && ind!=2 && ind!=8 ){
				if(!$chk($('inp-module_uid'))) return; // Codegen JS#00 Manually
				$('inp-module_uid').set('value',id);
				$('inp-module_id').set('value',dg1.getUserData(id,'module_id'));
				$('inp-module_name').set('value',dg1.getUserData(id,'module_name'));
				$('inp-sort_order').set('value',dg1.getUserData(id,'sort_order'));
				$('inp-isactive').set('value',dg1.getUserData(id,'isactive'));
				$('inp-description').set('value',dg1.getUserData(id,'description'));
				// #endregion Codegen : JS#01
				$('inp-create_by').set('value',dg1.getUserData(id,'create_by'));
				$('inp-create_byfn').set('text',dg1.getUserData(id,'create_byfn'));
				$('inp-create_at').set('text',dg1.getUserData(id,'create_date'));
				$('inp-create_by').set('value',dg1.getUserData(id,'update_by'));
				$('inp-update_byfn').set('text',dg1.getUserData(id,'update_byfn'));
				$('inp-update_at').set('text',dg1.getUserData(id,'update_date'));
				
				
				dg1__SwithcUIModf(2); 
			}
		});
		dg1.attachEvent('onCheckbox', function doOnCheck(rowId, cellInd, state) {                
			if(dgbox1_NoRow==true) return false;
			 else return true;
		});
		dg1.attachEvent("onRowAdded", function(rId){
			 
		}); 
		dg1.init();   
		
		dg2 = new dhtmlXGridObject('dgbox2');
		dg2.setImagePath('images/dhtmlxGrid/');
		// #region Codegen : JS#00 Manually
		dg2.setHeader(',No,Sub Module ID,Sub Module Name,Parent,Active,Sort Order,Description,Date of Record');
		dg2.setInitWidths('20,30,250,150,150,150,100,*,200');
		dg2.setColAlign('right,center,left,left,left,center,left,left,left');
		dg2.setColVAlign("middle,middle,middle,middle,middle,middle,middle,middle,middle");
		dg2.setColTypes('ch,ro,ro,ro,ro,ro,ro,ro,ro');
		dg2.setColSorting('int,int,str,str,str,str,str,str,str');
		dg2.enableResizing('false,true,true,true,true,true,true,true,true');
		dg2.enableTooltips('false,false,false,false,false,false,false,false,false');
		// #endregion Codegen : JS#00 Manually
		dg2.enableAutoHeight(true);
		dg2.enableMultiline(true);
		dg2.enableColSpan(true);
		dg2.setSkin('light');   
		dg2.attachEvent('onRowSelect', function doOnRowSelected(id,ind){
			if(id!='*' && ind!=0){
				if(!$chk($('inp-chd1-submodule_uid'))) return; // Codegen JS#00 Manually

				// #region Codegen : JS#01
				
				$('inp-chd1-submodule_uid').set('value',id);	
				$('inp-chd1-submodule_id').set('value',dg2.getUserData(id,'submodule_id'));
				$('inp-chd1-submodule_name').set('value',dg2.getUserData(id,'submodule_name'));
				$('inp-chd1-submodule_parent_uid').set('value',dg2.getUserData(id,'submodule_parent_uid'));
				$('inp-chd1-submodule_class').set('value',dg2.getUserData(id,'submodule_class'));
				$('inp-chd1-submodule_namespace').set('value',dg2.getUserData(id,'submodule_namespace'));
				$('inp-chd1-submodule_path').set('value',dg2.getUserData(id,'submodule_path'));
				$('inp-chd1-isactive').set('value',dg2.getUserData(id,'isactive'));
				$('inp-chd1-sort_order').set('value',dg2.getUserData(id,'sort_order'));
				$('inp-chd1-description').set('value',dg2.getUserData(id,'description'));
				// #endregion Codegen : JS#01
				$('inp-chd1-create_by').set('value',dg2.getUserData(id,'create_by'));
				$('inp-chd1-create_byfn').set('text',dg2.getUserData(id,'create_byfn'));
				$('inp-chd1-create_at').set('text',dg2.getUserData(id,'create_at'));
				$('inp-chd1-update_by').set('value',dg2.getUserData(id,'update_by'));
				$('inp-chd1-update_byfn').set('text',dg2.getUserData(id,'update_byfn'));
				$('inp-chd1-update_at').set('text',dg2.getUserData(id,'update_at'));	
				
				dg2__SwithcUIModf(2);		
				
			}
			
		});
		dg2.attachEvent('onCheckbox', function doOnCheck(rowId, cellInd, state) {                
			if(dgbox2_NoRow==true) return false;
			 else return true;
		});
		dg2.init(); 		
		
		
	}
	finally {
		dg1__Refresh(dgbox1_PageNum);
	}
	
}

function dg1__Refresh(pagenumber)
{
	
	var myRequest = new Request({
		url: regmodule_getByPage,
		data : {
			_token : $('_token').get('value'),
			rowsperpage : dgbox1_RowsPerPage,
			pagenum : pagenumber,
			//curentnav : dgbox1_CurrentNav,
			search : $('inp-dgbox1-normal-search-q').get('value')
		},
		method: 'get',
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none')
				$('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				dgbox1_SuspendRefresh=false;
				dgbox1_MaxRow = response.maxrow;
				dgbox1_NoRow = response.norow;
				dgbox1_PageNum = response.pagenum;
				if(dgbox1_NoRow)dg1.setColumnHidden(0,true);else dg1.setColumnHidden(0,false); 

				var tmpMaxPage = (response.maxrow/dgbox1_RowsPerPage);
				if(tmpMaxPage > tmpMaxPage.round(0)) dgbox1_PageNumMax = tmpMaxPage.round(0) + 1; else dgbox1_PageNumMax = tmpMaxPage.round(0);
				dg1.clearAll();
				dg1.parse(response.data, function(grid_obj,count){
					
					
	                 SetDataGridPagingNavControls(
							dgbox1_MaxRow, dgbox1_PageNum, dgbox1_RowsPerPage, dgbox1_PageNumMax,
							$('bt-dgbox1-nav-current-1'), $('bt-dgbox1-nav-first-1'), $('bt-dgbox1-nav-prev-1'), $('bt-dgbox1-nav-next-1'), $('bt-dgbox1-nav-last-1'),
							$('bt-dgbox1-nav-current-2'), $('bt-dgbox1-nav-first-2'), $('bt-dgbox1-nav-prev-2'), $('bt-dgbox1-nav-next-2'), $('bt-dgbox1-nav-last-2')
					);
					dgbox1_AddedRow = 0;
					//delete loaded xml file after successfull loads
				
					
			     });
				 //dgbox1_CurrentNav = ((dgbox1_RowsPerPage * (dgbox1_PageNum-1)).toFloat()+ 1);
			}
			//$('inp-dgbox1-normal-search-q').set('value',response.module_name);
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}


function dg1__Save()
{
	
	var myRequest = new Request({
		url: regmodule_AjaxIns,
		data : {
			_token : $('_token').get('value'),
			module_uid : $('inp-module_uid').get('value'),
			module_id : $('inp-module_id').get('value'),
			module_name : $('inp-module_name').get('value'),
			sort_order : $('inp-sort_order').get('value'),
			isactive : $('inp-isactive').get('value'),
			description : $('inp-description').get('value'),
			create_by : $('hd-user_id').get('value'),
			create_byfn : $('inp-create_byfn').get('text')
		},
		method: 'post',
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none') $('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				
				if(dgbox1_MaxRow==0){								
					dg1__SwithcUIModf(0); 
					dg1__Refresh(dgbox1_PageNum); 
					Resp = null; 				
				}
				dg1.addRow(response.ud_module_uid,response.data.split('#*#'),0);   
				dg1.setUserData(response.ud_module_uid,'module_uid',response.ud_module_uid);
				dg1.setUserData(response.ud_module_uid,'module_id',response.ud_module_id);
				dg1.setUserData(response.ud_module_uid,'module_name',response.ud_module_name);
				dg1.setUserData(response.ud_module_uid,'sort_order',response.ud_sort_order);
				dg1.setUserData(response.ud_module_uid,'isactive',response.ud_isactive);
				dg1.setUserData(response.ud_module_uid,'description',response.ud_description);
				
				dg1.setUserData(response.ud_module_uid,'create_by',response.ud_create_by);
				dg1.setUserData(response.ud_module_uid,'create_byfn',response.ud_create_byfn);
				dg1.setUserData(response.ud_module_uid,'create_at',response.ud_create_at);
				dg1.setUserData(response.ud_module_uid,'update_by',response.ud_update_by);
				dg1.setUserData(response.ud_module_uid,'update_byfn',response.ud_update_byfn);
				dg1.setUserData(response.ud_module_uid,'update_at',response.ud_update_at);
				
				if(dgbox1_PageNumMax == 0) dgbox1_PageNumMax = 1;
				dgbox1_MaxRow = dgbox1_MaxRow+1;
				dgbox1_AddedRow = dgbox1_AddedRow + 1;
				SetDataGridPagingInfoAfterInsert(dgbox1_MaxRow,dgbox1_PageNum,dgbox1_RowsPerPage,dgbox1_PageNumMax,$('bt-dgbox1-nav-current-1'),$('bt-dgbox1-nav-current-2'),dgbox1_AddedRow);
				
				dg1.selectRow(0,false,false,false);
				dg1__SwithcUIModf(0);	
				setTimeout("bodyscroll.toTop()", 255);		
				MessegeInfoShow('Data successfully saved');					
			} 
			else 
			{ alert(response.ErrorMessege); }
			
			//$('inp-dgbox1-normal-search-q').set('value',response.module_name);
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}

function dg1__Update()
{
	
	var myRequest = new Request({
		url: regmodule_AjaxUpd,
		data : {
			_token : $('_token').get('value'),
			module_uid : $('inp-module_uid').get('value'),
			module_id : $('inp-module_id').get('value'),
			module_name : $('inp-module_name').get('value'),
			sort_order : $('inp-sort_order').get('value'),
			isactive : $('inp-isactive').get('value'),
			description : $('inp-description').get('value'),
			
			create_by : $('inp-create_by').get('value'),
			create_byfn : $('inp-create_byfn').get('text'),
			craete_at : $('inp-create_at').get('text'),
			update_by : $('hd-user_id').get('value'),
			update_byfn : $('inp-update_byfn').get('text'),
			
			rowidx : dgbox1_CurrentRecNum
		},
		method: 'post',
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none') $('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				
				var idx = dg1.getRowIndex(response.ud_module_uid); 
				if(idx == -1){alert('row not found');return;}
				dg1.deleteRow(response.ud_module_uid);
				dg1.addRow(response.ud_module_uid,response.data.split('#*#'),idx);   
				
				dg1.setUserData(response.ud_module_uid,'module_uid',response.ud_module_uid);
				dg1.setUserData(response.ud_module_uid,'module_id',response.ud_module_id);
				dg1.setUserData(response.ud_module_uid,'module_name',response.ud_module_name);
				dg1.setUserData(response.ud_module_uid,'sort_order',response.ud_sort_order);
				dg1.setUserData(response.ud_module_uid,'isactive',response.ud_isactive);
				dg1.setUserData(response.ud_module_uid,'description',response.ud_description);
				
				dg1.setUserData(response.ud_module_uid,'create_by',response.ud_create_by);
				dg1.setUserData(response.ud_module_uid,'create_byfn',response.ud_create_byfn);
				dg1.setUserData(response.ud_module_uid,'create_at',response.ud_create_at);
				dg1.setUserData(response.ud_module_uid,'update_by',response.ud_update_by);
				dg1.setUserData(response.ud_module_uid,'update_byfn',response.ud_update_byfn);
				dg1.setUserData(response.ud_module_uid,'update_at',response.ud_update_at);
				
				dg1.selectRow(idx,false,false,false);
				dg1__SwithcUIModf(0);	
				setTimeout("bodyscroll.toTop()", 255);		
				MessegeInfoShow('Data successfully saved');					
			} 
			else 
			{ alert(response.ErrorMessege); }
			
			//$('inp-dgbox1-normal-search-q').set('value',response.module_name);
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}

function dg1__Delete(CheckedRows)
{
	var myRequest = new Request({
		url: regmodule_AjaxDel,
		method: 'delete',
		data : {
			_token : $('_token').get('value'),
			module_uid : CheckedRows
		},
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none') $('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				dg1__Refresh(dgbox1_PageNum);
				setTimeout("bodyscroll.toTop()", 255);
			} 
			else 
			{ alert(response.ErrorMessege); }
			
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}


function dg1__SwithcUIModf(UIElmnt){
	switch (UIElmnt){
		case 0:
			//show gridbox, hide editor
			// ######## gridbox UI
			$('dgbox1-grid-modf-top').setStyle('display', 'block');	
			$('dgbox1-grid-main').setStyle('display', 'block');
			$('dgbox1-grid-modf-bottom').setStyle('display', 'block');
			// ######## editor UI
			$('dgbox1-editor-head').setStyle('display', 'none');		
			$('dgbox1-editor-main').setStyle('display', 'none');	
			$('dgbox1-editor-modf-bottom').setStyle('display', 'none');
			$('top-pan-filter').setStyle('display','block');
			$('top-bottom-pan-filter').setStyle('display','inline');
			
			
			// ######## editor UI field
			//dgbox2_Refresh_Trigger = 1; // untuk refresh dg2 ketik klik tab 2
			
			break;
		case 1:
        	//hide gridbox, show editor add
			// ######## gridbox UI
			$('dgbox1-grid-modf-top').setStyle('display', 'none');	
			$('dgbox1-grid-main').setStyle('display', 'none');
			$('dgbox1-grid-modf-bottom').setStyle('display', 'none');
			// ######## editor UI
			$('rgn-dgbox1-editor-title').set('text', 'New '+dgbox1_Title);
			$('dgbox1-editor-head').setStyle('display', 'block');	
			$('dgbox1-editor-modf-top-1').setStyle('visibility', 'hidden');
			$('dgbox1-editor-modf-top-2').setStyle('visibility', 'hidden');			
			$('dgbox1-editor-main').setStyle('display', 'block');	
			//$('dgbox1-editor-modf-bottom').setStyle('display', 'block');	
			
			
			$('top-pan-filter').setStyle('display','none');
			$('top-bottom-pan-filter').setStyle('display','none');
			// ######## editor UI field, add
			dg1__SwithcUIModfTab(1);
			dgbox1_CurrentRecNum = 0;
			
			// #region Codegen : JS#04
			$('inp-module_uid').set('value','');
			$('inp-module_id').set('value','');
			$('inp-module_name').set('value','');
			$('inp-sort_order').set('value','');
			$('inp-isactive').set('value','True');
			$('inp-description').set('value','');
			// #endregion Codegen : JS#04
			$('inp-create_byfn').set('text','');
			$('inp-create_at').set('text','');
			$('inp-update_byfn').set('text','');
			$('inp-update_at').set('text','');
			setTimeout("bodyscroll.toTop()", 255);
			break;
		case 2:
			//hide gridbox, show editor edit
			// ######## gridbox UI
			$('dgbox1-grid-modf-top').setStyle('display', 'none');	
			$('dgbox1-grid-main').setStyle('display', 'none');
			$('dgbox1-grid-modf-bottom').setStyle('display', 'none');
			// ######## editor UI
			$('rgn-dgbox1-editor-title').set('text', 'Edit '+dgbox1_Title);
			$('dgbox1-editor-head').setStyle('display', 'block');	
		
			$('dgbox1-editor-modf-top-1').setStyle('visibility', 'visible');
			$('dgbox1-editor-modf-top-2').setStyle('visibility', 'visible');	
			$('dgbox1-editor-main').setStyle('display', 'block');	
			
			$('top-pan-filter').setStyle('display','none');
			$('top-bottom-pan-filter').setStyle('display','none');
			//$('dgbox1-editor-modf-bottom').setStyle('display', 'block');			
			// ######## editor UI field, edit
			
			dg1__SwithcUIModfTab(1);
			
			//$('inp-DATE_HOLIDAY').focus(); // Codegen JS#00 Manually
			var cellById = dg1.cellById($('inp-module_uid').get('value'), 2).getValue();
			dgbox1_CurrentRecNum = cellById.replace("<b>","").replace("</b>","");  // Codegen JS#00 Manually	
			
			setTimeout("bodyscroll.toTop()", 255);
			break;
	}
    dgbox1_UiState = UIElmnt;
} 	


function dg1__SwithcUIModfTab(tabIdx){
	//alert($('inp-module_uid').get('value')+"---"+dgbox2_Refresh_Trigger);
	//if(dgbox1_editor_activetabs != tabIdx){
		switch(tabIdx){	
			case 1:
				// bt save cancel
				$('bt-dgbox1-editor-save-1').setStyle('visibility', 'visible');
				$('bt-dgbox1-editor-save-2').setStyle('visibility', 'visible');
				$('bt-dgbox1-editor-delete-single-1').setStyle('visibility', 'visible');
				$('bt-dgbox1-editor-delete-single-2').setStyle('visibility', 'visible');
				// tab1
				//$('dgbox1-editor-tab1').set('class', 'active');
				$('dgbox1-editor-tab1-content').setStyle('display', 'block');
				$('rgn-dgbox1-modf-foot').setStyle('display', 'block');
				// tab2
				//$('dgbox1-editor-tab2').set('class', 'inactive');        
				$('dgbox1-editor-tab2-content').setStyle('display', 'none');
				dgbox1_editor_activetabs = tabIdx;
				break;	
			case 2:
				//if(dgbox1_UiState == 2){ // hanya edit mode yg bisa ke tab2
					// bt save cancel
					$('bt-dgbox1-editor-save-1').setStyle('visibility', 'hidden');
					$('bt-dgbox1-editor-save-2').setStyle('visibility', 'hidden');
					$('bt-dgbox1-editor-delete-single-1').setStyle('visibility', 'hidden');
					$('bt-dgbox1-editor-delete-single-2').setStyle('visibility', 'hidden');
					// tab1
					//$('dgbox1-editor-tab1').set('class', 'inactive');
					$('dgbox1-editor-tab1-content').setStyle('display', 'none');
					$('rgn-dgbox1-modf-foot').setStyle('display', 'none');
					// tab2
					//$('dgbox1-editor-tab2').set('class', 'active');
					$('dgbox1-editor-tab2-content').setStyle('display', 'block');
					
					dgbox1_editor_activetabs = tabIdx;
					//if(dg2.getRowsNum()==0){dg2__Refresh(1)};
					
					if($('inp-module_uid').get('value') != dgbox2_Refresh_Trigger)
					{
						dg2__Refresh(1);
						dgbox2_Refresh_Trigger = 0;
					}
					dgbox2_Refresh_Trigger = $('inp-module_uid').get('value');
				//}
				break;
		}
	//}
}

function viewDetail(module_uid,module_id,module_name)
{
	
	
	
	// copas dari function dg1__SwithcUIModf(2);
	$('inp-module_uid').set('value',module_uid);
	//dgbox2_Refresh_Trigger = 1; // untuk refresh dg2 ketik klik tab 2
	
	// ######## gridbox UI
	$('dgbox1-grid-modf-top').setStyle('display', 'none');	
	$('dgbox1-grid-main').setStyle('display', 'none');
	$('dgbox1-grid-modf-bottom').setStyle('display', 'none');
	// ######## editor UI
	$('rgn-dgbox1-editor-title').set('text', 'Detail '+dgbox1_Title);
	$('dgbox1-editor-head').setStyle('display', 'block');	

	$('dgbox1-editor-modf-top-1').setStyle('visibility', 'visible');
	$('dgbox1-editor-modf-top-2').setStyle('visibility', 'visible');	
	$('dgbox1-editor-main').setStyle('display', 'block');	
	
	$('top-pan-filter').setStyle('display','none');
	$('top-bottom-pan-filter').setStyle('display','none');			
	// ######## editor UI field, edit
	
	$('lbl-module_id').set('html',module_id);
	$('lbl-module_name').set('html',module_name);
	
	dg1__SwithcUIModfTab(2);
	
	var cellById = dg1.cellById($('inp-module_uid').get('value'), 2).getValue();
	dgbox1_CurrentRecNum = cellById.replace("<b>","").replace("</b>","");  // Codegen JS#00 Manually	
		
	
}


function dg2__Refresh(pagenumber)
{
	
	var myRequest = new Request({
		url: regmodule_getByPageChld,
		data : {
			_token : $('_token').get('value'),
			rowsperpage : dgbox2_RowsPerPage,
			pagenum : pagenumber,
			//curentnav : dgbox2_CurrentNav,
			search : $('inp-dgbox2-normal-search-q').get('value'),
			module_uid : $('inp-module_uid').get('value')
		},
		method: 'get',
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none')
				$('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				if($defined(response.selectopt))
				{
					$('inp-chd1-rgn-submodule_parent_uid').empty();
					$('inp-chd1-rgn-submodule_parent_uid').set('html',response.selectopt);
				}
				dgbox2_MaxRow = response.maxrow;
				dgbox2_NoRow = response.norow;
				dgbox2_PageNum = response.pagenum;
				if(dgbox2_NoRow)dg2.setColumnHidden(0,true);else dg2.setColumnHidden(0,false); 

				var tmpMaxPage = (response.maxrow/dgbox2_RowsPerPage);
				if(tmpMaxPage > tmpMaxPage.round(0)) dgbox2_PageNumMax = tmpMaxPage.round(0) + 1; else dgbox2_PageNumMax = tmpMaxPage.round(0);
				dg2.clearAll();
				dg2.parse(response.data, function(grid_obj,count){
	                 SetDataGridPagingNavControls(
							dgbox2_MaxRow, dgbox2_PageNum, dgbox2_RowsPerPage, dgbox2_PageNumMax,
							$('bt-dgbox2-nav-current-1'), $('bt-dgbox2-nav-first-1'), $('bt-dgbox2-nav-prev-1'), $('bt-dgbox2-nav-next-1'), $('bt-dgbox2-nav-last-1'),
							$('bt-dgbox2-nav-current-2'), $('bt-dgbox2-nav-first-2'), $('bt-dgbox2-nav-prev-2'), $('bt-dgbox2-nav-next-2'), $('bt-dgbox2-nav-last-2')
					);
					dgbox2_AddedRow = 0;
			     });
			}
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}


function dg2__Save()
{
	var submodule_parent_name = "";
	if($('inp-chd1-submodule_parent_uid').get('value') != "")
	{
		submodule_parent_name = $('inp-chd1-submodule_parent_uid').options[$('inp-chd1-submodule_parent_uid').selectedIndex].text;
	}
	
	var myRequest = new Request({
		url: regmodule_AjaxInsChld,
		data : {
			_token : $('_token').get('value'),
			module_uid : $('inp-module_uid').get('value'),
			submodule_uid : $('inp-chd1-submodule_uid').get('value'),
			submodule_id : $('inp-chd1-submodule_id').get('value'),
			submodule_name : $('inp-chd1-submodule_name').get('value'),
			submodule_parent_uid : $('inp-chd1-submodule_parent_uid').get('value'),
			submodule_parent_name : submodule_parent_name,
			submodule_class : $('inp-chd1-submodule_class').get('value'),
			submodule_namespace : $('inp-chd1-submodule_namespace').get('value'),
			submodule_path : $('inp-chd1-submodule_path').get('value'),
			description : $('inp-chd1-description').get('value'),
			sort_order : $('inp-chd1-sort_order').get('value'),
			isactive : $('inp-chd1-isactive').get('value'),
			
			create_by : $('hd-user_id').get('value'),
			create_byfn : $('inp-create_byfn').get('text')
		},
		method: 'post',
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none') $('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				
				if(dgbox2_MaxRow==0){								
					dg2__SwithcUIModf(0); 
					dg2__Refresh(dgbox1_PageNum); 
					Resp = null; 				
				}
				dg2.addRow(response.ud_submodule_uid,response.data.split('#*#'),0);   
				dg2.setUserData(response.ud_submodule_uid,'submodule_uid',response.ud_submodule_uid);
				dg2.setUserData(response.ud_submodule_uid,'module_uid',response.ud_module_uid);
				dg2.setUserData(response.ud_submodule_uid,'submodule_id',response.ud_submodule_id);
				dg2.setUserData(response.ud_submodule_uid,'submodule_name',response.ud_submodule_name);
				dg2.setUserData(response.ud_submodule_uid,'submodule_parent_uid',response.ud_submodule_parent_uid);
				dg2.setUserData(response.ud_submodule_uid,'submodule_class',response.ud_submodule_class);
				dg2.setUserData(response.ud_submodule_uid,'submodule_namespace',response.ud_submodule_namespace);
				dg2.setUserData(response.ud_submodule_uid,'submodule_path',response.ud_submodule_path);
				dg2.setUserData(response.ud_submodule_uid,'sort_order',response.ud_sort_order);
				dg2.setUserData(response.ud_submodule_uid,'isactive',response.ud_isactive);
				dg2.setUserData(response.ud_submodule_uid,'description',response.ud_description);
				
				dg2.setUserData(response.ud_submodule_uid,'create_by',response.ud_create_by);
				dg2.setUserData(response.ud_submodule_uid,'create_byfn',response.ud_create_byfn);
				dg2.setUserData(response.ud_submodule_uid,'create_at',response.ud_create_at);
				dg2.setUserData(response.ud_submodule_uid,'update_by',response.ud_update_by);
				dg2.setUserData(response.ud_submodule_uid,'update_byfn',response.ud_update_byfn);
				dg2.setUserData(response.ud_submodule_uid,'update_at',response.ud_update_at);
				
				if(dgbox2_PageNumMax == 0) dgbox2_PageNumMax = 1;
				dgbox2_MaxRow = dgbox2_MaxRow+1;
				dgbox2_AddedRow = dgbox2_AddedRow + 1;
				SetDataGridPagingInfoAfterInsert(dgbox2_MaxRow,dgbox2_PageNum,dgbox2_RowsPerPage,dgbox2_PageNumMax,$('bt-dgbox2-nav-current-1'),$('bt-dgbox2-nav-current-2'),dgbox2_AddedRow);
				
				dg2.selectRow(0,false,false,false);
				dg2__SwithcUIModf(0);	
				setTimeout("bodyscroll.toTop()", 255);		
				MessegeInfoShow('Data successfully saved');					
			} 
			else 
			{ alert(response.ErrorMessege); }
			
		
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}

function dg2__Update()
{
	
	var submodule_parent_name="";
	if($('inp-chd1-submodule_parent_uid').get('value') != "")
	{
		submodule_parent_name = $('inp-chd1-submodule_parent_uid').options[$('inp-chd1-submodule_parent_uid').selectedIndex].text;
	}
	
	
	var myRequest = new Request({
		url: regmodule_AjaxUpdChld,
		data : {
			_token : $('_token').get('value'),
			module_uid : $('inp-module_uid').get('value'),
			submodule_uid : $('inp-chd1-submodule_uid').get('value'),
			submodule_id : $('inp-chd1-submodule_id').get('value'),
			submodule_name : $('inp-chd1-submodule_name').get('value'),
			submodule_parent_uid : $('inp-chd1-submodule_parent_uid').get('value'),
			submodule_parent_name : submodule_parent_name,
			submodule_class : $('inp-chd1-submodule_class').get('value'),
			submodule_namespace : $('inp-chd1-submodule_namespace').get('value'),
			submodule_path : $('inp-chd1-submodule_path').get('value'),
			description : $('inp-chd1-description').get('value'),
			sort_order : $('inp-chd1-sort_order').get('value'),
			isactive : $('inp-chd1-isactive').get('value'),
			
			create_by : $('inp-chd1-create_by').get('value'),
			create_byfn : $('inp-chd1-create_byfn').get('text'),
			craete_at : $('inp-chd1-create_at').get('text'),
			update_by : $('hd-user_id').get('value'),
			update_byfn : $('inp-chd1-update_byfn').get('text'),
			
			rowidx : dgbox2_CurrentRecNum
		},
		method: 'post',
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none') $('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				
				var idx = dg2.getRowIndex(response.ud_submodule_uid); 
				if(idx == -1){alert('row not found');return;}
				dg2.deleteRow(response.ud_submodule_uid);
				dg2.addRow(response.ud_submodule_uid,response.data.split('#*#'),idx);   
				
				dg2.setUserData(response.ud_submodule_uid,'submodule_uid',response.ud_submodule_uid);
				dg2.setUserData(response.ud_submodule_uid,'module_uid',response.ud_module_uid);
				dg2.setUserData(response.ud_submodule_uid,'submodule_id',response.ud_submodule_id);
				dg2.setUserData(response.ud_submodule_uid,'submodule_name',response.ud_submodule_name);
				dg2.setUserData(response.ud_submodule_uid,'submodule_parent_uid',response.ud_submodule_parent_uid);
				dg2.setUserData(response.ud_submodule_uid,'submodule_class',response.ud_submodule_class);
				dg2.setUserData(response.ud_submodule_uid,'submodule_namespace',response.ud_submodule_namespace);
				dg2.setUserData(response.ud_submodule_uid,'submodule_path',response.ud_submodule_path);
				dg2.setUserData(response.ud_submodule_uid,'sort_order',response.ud_sort_order);
				dg2.setUserData(response.ud_submodule_uid,'isactive',response.ud_isactive);
				dg2.setUserData(response.ud_submodule_uid,'description',response.ud_description);
				
				dg1.setUserData(response.ud_submodule_uid,'create_by',response.ud_create_by);
				dg1.setUserData(response.ud_submodule_uid,'create_byfn',response.ud_create_byfn);
				dg1.setUserData(response.ud_submodule_uid,'create_at',response.ud_create_at);
				dg1.setUserData(response.ud_submodule_uid,'update_by',response.ud_update_by);
				dg1.setUserData(response.ud_submodule_uid,'update_byfn',response.ud_update_byfn);
				dg1.setUserData(response.ud_submodule_uid,'update_at',response.ud_update_at);
				
				dg2.selectRow(idx,false,false,false);
				dg2__SwithcUIModf(0);	
				setTimeout("bodyscroll.toTop()", 255);		
				MessegeInfoShow('Data successfully saved');					
			} 
			else 
			{ alert(response.ErrorMessege); }

		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}

function dg2__Delete(CheckedRows)
{
	var myRequest = new Request({
		url: regmodule_AjaxDelChld,
		method: 'delete',
		data : {
			_token : $('_token').get('value'),
			module_uid: $('inp-module_uid').get('value'),
			submodule_uid : CheckedRows
		},
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none') $('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.success)
			{
				dg2__Refresh(dgbox2_PageNum);
				setTimeout("bodyscroll.toTop()", 255);
			} 
			else 
			{ alert(response.ErrorMessege); }
			
			
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}

function dg2__SwithcUIModf(UIElmnt){

	switch (UIElmnt){
		case 0:
			//show gridbox, hide editor
			// ######## gridbox UI
			$('dgbox2-grid-modf-top').setStyle('display', 'block');	
			$('dgbox2-grid-main').setStyle('display', 'block');
			$('dgbox2-grid-modf-bottom').setStyle('display', 'none');
			// ######## editor UI	
			$('dgbox2-editor-modf-top').setStyle('display', 'none');	
			$('dgbox2-editor-main').setStyle('display', 'none');	
			$('dgbox2-editor-modf-bottom').setStyle('display', 'none');
			// ######## editor UI field
			
			
			break;
		case 1:
        	//hide gridbox, show editor add
			// ######## gridbox UI
			$('dgbox2-grid-modf-top').setStyle('display', 'none');	
			$('dgbox2-grid-main').setStyle('display', 'none');
			$('dgbox2-grid-modf-bottom').setStyle('display', 'none');
			// ######## editor UI
			$('rgn-dgbox2-editor-title').set('text', 'New '+dgbox2_Title);
			$('dgbox2-editor-modf-top').setStyle('display', 'block');
			$('dgbox2-editor-modf-top-1').setStyle('visibility', 'hidden');
			$('dgbox2-editor-modf-top-2').setStyle('visibility', 'hidden');			
			$('dgbox2-editor-main').setStyle('display', 'block');	
			$('dgbox2-editor-modf-bottom').setStyle('display', 'block');
			// ######## editor UI field, add
			dgbox2_CurrentRecNum = 0;

			// #region Codegen : JS#04
			
			$('inp-chd1-submodule_id').set('value','');
			$('inp-chd1-submodule_name').set('value','');
			//$('inp-chd1-submodule_parent_uid').set('value','00000000-0000-0000-0000-000000000000');
			$('inp-chd1-submodule_class').set('value','');
			$('inp-chd1-submodule_namespace').set('value','');
			$('inp-chd1-submodule_path').set('value','');
			$('inp-chd1-sort_order').set('value','');
			$('inp-chd1-isactive').set('value','True');
			$('inp-chd1-description').set('value','');
			// Codegen JS#00 Manually
			// #endregion Codegen : JS#04
			$('inp-chd1-create_byfn').set('text','');
			$('inp-chd1-create_at').set('text','');
			$('inp-chd1-update_byfn').set('text','');
			$('inp-chd1-update_at').set('text','');
			setTimeout("bodyscroll.start(0,$('bt-dgbox2-editor-cancel-1').getPosition().y)", 255);
			break;
		case 2:
			//hide gridbox, show editor edit
			// ######## gridbox UI
			$('dgbox2-grid-modf-top').setStyle('display', 'none');	
			$('dgbox2-grid-main').setStyle('display', 'none');
			$('dgbox2-grid-modf-bottom').setStyle('display', 'none');
			// ######## editor UI
			$('rgn-dgbox2-editor-title').set('text', 'Edit '+dgbox2_Title);
			$('dgbox2-editor-modf-top').setStyle('display', 'block');
			$('dgbox2-editor-modf-top-1').setStyle('visibility', 'visible');
			$('dgbox2-editor-modf-top-2').setStyle('visibility', 'visible');	
			$('dgbox2-editor-main').setStyle('display', 'block');	
			$('dgbox2-editor-modf-bottom').setStyle('display', 'block');		
			// ######## editor UI field, edit

			
			dgbox2_CurrentRecNum = dg2.cellById($('inp-chd1-submodule_uid').get('value'), 1).getValue();  // Codegen JS#00 Manually
			setTimeout("bodyscroll.start(0,$('bt-dgbox2-editor-cancel-1').getPosition().y)", 255);
			break;
	}
    dgbox2_UiState = UIElmnt;
} 
	

