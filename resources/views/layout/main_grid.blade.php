
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Map</title>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />	
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
	<meta http-equiv="PRAGMA" content="NO-CACHE" />
	<meta name="Developer" content="Nova Adryanto"/>
	<meta name="Developer-Email" content="novlog@gmail.com"/>
	<link rel="shortcut icon" href= "" />
	<style type="text/css" >
		
		a.navmenuLink:link { text-decoration: underline; font-weight:normal !important; font-style:normal !important; color: white; }
		a.navmenuLink:visited { text-decoration: underline; font-weight:normal !important; font-style:normal !important; color: white; }
		a.navmenuLink:hover { text-decoration: underline; font-weight:normal !important; font-style:normal !important; color: white; }
		a.navmenuLink:active { text-decoration: underline; font-weight:normal !important; font-style:normal !important; color: white; }	

		a.homemenuLink:link { text-decoration: underline; font-weight:normal !important; font-style:normal !important; color: blue; }
		a.homemenuLink:visited { text-decoration: underline; font-weight:normal !important; font-style:normal !important; color: blue; }
		a.homemenuLink:hover { text-decoration: underline; font-weight:normal !important; font-style:normal !important; color: blue; }
		a.homemenuLink:active { text-decoration: underline; font-weight:normal !important; font-style:normal !important; color: blue; }	
		/*
		/css/calendar-eightysix-v1.1-default.css
		/css/calendar-eightysix-v1.1-vista.css
		/css/calendar-eightysix-v1.1-vista-clean.css
		*/		
	</style>
	<link type="text/css" rel="stylesheet" media="all" href="{{asset('css/Default.css')}}" />
	<link type="text/css" rel="stylesheet" media="all" href="{{asset('css/menumatic/MenuMatic.css')}}" media="screen" /> 
	<link rel="stylesheet" type="text/css" media="all" href="{{asset('css/dhtmlxGrid/dhtmlxgrid.css')}}" />
	<link rel="stylesheet" type="text/css" media="all" href="{{asset('css/dhtmlxGrid/dhtmlxgrid_skins.css')}}" />
	<link type="text/css" rel="stylesheet" media="all" href="{{asset('css/LightFace/LightFace.css')}}" />

	@if ($js === 'mootools')
		<script src="{{ asset('js/Mootools/mootools-1.2.4-core.js') }} " type="text/javascript"></script>
		<script src="{{ asset('js/Mootools/mootools-1.2.4.4-more.js') }}" type="text/javascript"></script>
		<script src="{{ asset('js/FloatingTips.js')}}" type="text/javascript"></script>
		<script src="{{ asset('js/FloatingTips.js')}}" type="text/javascript"></script>
		<script src="{{ asset('js/calendar-eightysix-v1.1.js')}}" type="text/javascript"></script>
		<script src="{{ asset('js/lightface/LightFace.js')}}" type="text/javascript"></script>
		<script src="{{ asset('js/lightface/LightFace.IFrame.js')}}" type="text/javascript"></script>
		<script src="{{ asset('js/lightface/LightFace.Image.js')}}" type="text/javascript"></script>
		<script src="{{ asset('js/lightface/LightFace.Request.js')}}" type="text/javascript"></script>

		
	@elseif ($js === '')
		I have two script!
	@else
		I don't have any script!
	@endif
	
	

	<script src="{{asset('js/dhtmlx/dhtmlxcommon_v.2.5_090904.js')}}" type="text/javascript"></script>
	<script src="{{asset('js/dhtmlx/dhtmlxgrid_v.2.5_090904.js')}}" type="text/javascript"></script>
	<script src="{{asset('js/dhtmlx/dhtmlxgridcell_v.2.5_090904.js')}}" type="text/javascript"></script>
	<script src="{{asset('js/dhtmlx/dhtmlxtreegrid_v.2.1_90226.js')}}" type="text/javascript"></script>
	<script src="{{asset('js/common.js')}}" type="text/javascript"></script>
	

	

</head>
<body>
	<form id="form1" >
			
			<div >
				<a id="ach-top" name="ach-top"></a>	
				<iframe id="frm-CLNSVC" style="z-index:-1;position:absolute;" frameborder="0" scrolling="no" width="1px" height="1px">
					<p>Your browser does not support iframes.</p>
				</iframe>
				 <table cellspacing="0" cellpadding="0" width="100%" border="0px" align="center">

                    <!-- #REGION: MAIN MENU CONTAINER -->
					<tr valign="middle">
					  <td class="pg-head-defbg" style="height:15px; width: 8px;">
					  </td>
						<td id="main_menu" class="pg-head-defbg" style="height:15px;" align="center">							
							<div id="page-header" >
								
								<div id="nav_wrapper" class="important-left left-align" style="height: 15px;" >
									
									<ul id="nav" class="left-align" >
										<div id="logo">AGTS</div>
										<li><a>Dashboard</a></li>
										<li><a>Administrator</a>
											<ul>
												<li><a>Register Module</a></li>
												<li><a>Manage Menu</a></li>
												<li><a>User</a></li>
												<li><a>Roles</a></li>
											</ul>
										</li>
										<li><a>Dummy 2</a>
											<ul>
												<li><a>Dummy 2 a</a></li>
												<li><a>Dummy 2 b </a>
													<ul>
														<li><a>Dummy 2 b-a</a></li>
													</ul>
												</li>
											</ul>
										</li>
										<li><a>Dummy 3</a></li>
									</ul>
									
									
									

								</div>
								<div class="important-right right-align" style="padding:7px 0px;height: 15px;font-size: 12px;color:white;">
									<span class="bold" style="color:#CCE9FF;"><asp:Label ID="LbUserFullname" runat="server" Text="Label"></asp:Label></span>
									<!--
									|
									<a href = "<%=Anov.Site.Helper.GetApplicationPath(Request) + "/MyNotes/ManageMyNotes.aspx" %>" >My Notes</a>
									-->
									|
									<a class="navmenuLink" href = "#" >My Profile</a>
									|
									<a class="navmenuLink" href = "#" >Log out</a>
								</div>
							</div>
							<!-- 
							<div style="height: 32px; float:right;" >
									<li><a id="help-" href="#"><img src="images/icons/menu-help.png" alt="help">&nbsp;Help</a></li>
							</div>		
							-->
						</td>
					  <td class="pg-head-defbg" style="height:15px; width: 8px;">
					  </td>
					</tr>
                    <!-- #ENDREGION: MAIN MENU CONTAINER -->
				 </table>
			</div>
			<div id="hde-loading-info"  style="display:none;">
				Processing... 
			</div>				
			<div style="position:absolute;width:100%;height:141px;z-index:-10000;" class="smcCenter" ></div>					
			<div id="main_content" style="margin:0px; overflow-x: hidden  !important; overflow-y: hidden  !important; " >
				
				<input  type="hidden"  id="hd-user_id" value="fn14admin">
				
				<section class="content">
					@yield('content');
				</section>

			</div>	
			
			<div style="  right: 10px; bottom:1px;left:10px;">
				<table cellspacing="0" cellpadding="0" width="100%" border="0px" align="center">

                    <!-- #REGION: FOOTER CONTAINER -->
					<tr>
					  <td class="smcBottomLeft" style="width: 8px; height:36px;">
					  </td>
						<td class="smcBottom" style="height:36px;" valign="middle">
							<div class="important-left top-pad-5 copyr small-txt" >
                            	<a name="31a" id="31a"></a>
                            	Copyright &copy; <asp:Label ID="LbCopyrightBy" runat="server" Text="Label"></asp:Label>
							</div>
							<div class="important-right right-align top-pad-5 copyr small-txt" >
                            	Runtime Version: <asp:Label ID="LbMainBinary" runat="server" Text="Label"></asp:Label>
							</div>
							
						</td>
				      <td class="smcBottomRight" style="width: 8px; height:36px;">
				      </td>
					</tr>
                    <!-- #ENDREGION: FOOTER CONTAINER -->
				 </table>
			</div>		
	</form>
</body>
</html>