<?php

namespace App\Http\Controllers;
use File;
use Session;
use App\Classes\HelperCustom;
use App\Models\UserSys;
//use Ramsey\Uuid\Uuid;
use DateTime;

use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Contracts\Routing\ResponseFactory;

class Result
{
	public $success;
	public $errormessage;
	public $url;
	public $data;
	public $rowsperpage = 0; 	
	public $pagenum=0;
	public $maxrow=0;
	public $norow=false;
	public $selectopt;

}

class InsertUpdate
{
	public $success;
	public $errormessage;
	public $data;
	public $recnum;
	
	public $ud_user_id;		
	public $ud_user_name;		
	public $ud_password;
	public $ud_user_type;
	public $ud_email;
	public $ud_phone;
	public $ud_isactive;
	
	public $ud_create_by;
	public $ud_create_byfn;
	public $ud_create_at;
	public $ud_update_by;
	public $ud_update_byfn;
	public $ud_update_at;
	
}

class DeleteResult
{
	public $success;
	public $errormessage;
	public $deletedid;
}	

class UserSysController extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
	
	public function index()
	{
		
		return view('module.usersys.index',[
			'js' => 'mootools',
        ]);
		
	}
	
	public function show()
	{
		$Res = new Result();
		$RowsPerPage = Input::get('rowsperpage');
		$PageNum = Input::get('pagenum');
		$CurentNav = Input::get('curentnav');
		$search = Input::get('search');
		$MaxRow = 0; 
		$offset = ($PageNum-1)*$RowsPerPage;
		
		// prepare file base variable
		$sbRowBody = File::get(storage_path('common/rowsbody.txt'));
		$sbRowItems ='';
		$getReplaceRowBody = '';
		$RowItemTemplate = view('module.usersys.row');
		$RowEmpty = view('module.usersys.rowEmpty');
				
		$usersys = UserSys::getByPage($RowsPerPage,$offset,$search);
		
		foreach($usersys->maxrow as $key=>$value)
		{
			$MaxRow = $value['maxrow'];
		}
		if(!empty($usersys->selectrow))
		{
			foreach($usersys->selectrow as $users)
			{
				
				$isactive;
				if($users['isactive'] == 'Aktif'){$isactive = 1;}else{$isactive = 0;} 
				
				$sbRowItems = $sbRowItems.$RowItemTemplate;
				$sbRowItems = str_replace('#ud_user_id#',$users['user_id'],$sbRowItems);
				$sbRowItems = str_replace('#ud_user_name#',$users['user_name'],$sbRowItems);
				$sbRowItems = str_replace('#ud_password#',$users['password'],$sbRowItems);
				$sbRowItems = str_replace('#ud_user_type#',$users['user_type'],$sbRowItems);
				$sbRowItems = str_replace('#ud_email#',$users['email'],$sbRowItems);
				$sbRowItems = str_replace('#ud_phone#',$users['phone'],$sbRowItems);
				$sbRowItems = str_replace('#ud_isactive#',$isactive,$sbRowItems);
				
				$sbRowItems = str_replace('#ud_create_by#',$users['create_by'],$sbRowItems);
				$sbRowItems = str_replace('#ud_create_byfn#',$users['create_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#ud_create_at#',$users['create_at'],$sbRowItems);
				$sbRowItems = str_replace('#ud_update_by#',$users['update_by'],$sbRowItems);
				$sbRowItems = str_replace('#ud_update_byfn#',$users['update_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#ud_update_at#',$users['update_at'],$sbRowItems);

				
				$sbRowItems = str_replace('#recnum#',$users['recnum'],$sbRowItems);
				$sbRowItems = str_replace('#user_id#',$users['user_id'],$sbRowItems);
				$sbRowItems = str_replace('#user_name#',$users['user_name'],$sbRowItems);
				$sbRowItems = str_replace('#password#',$users['password'],$sbRowItems);
				$sbRowItems = str_replace('#user_type_var#',$users['user_type_var'],$sbRowItems);
				$sbRowItems = str_replace('#email#',$users['email'],$sbRowItems);
				$sbRowItems = str_replace('#phone#',$users['phone'],$sbRowItems);
				$sbRowItems = str_replace('#isactive#',$users['isactive'],$sbRowItems);
				
				$sbRowItems = str_replace('#create_by#',$users['create_by'],$sbRowItems);
				$sbRowItems = str_replace('#create_byfn#',$users['create_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#create_at#',$users['create_at'],$sbRowItems);
				$sbRowItems = str_replace('#update_by#',$users['update_by'],$sbRowItems);
				$sbRowItems = str_replace('#update_byfn#',$users['update_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#update_at#',$users['update_at'],$sbRowItems);
				
			}
			
			$getReplaceRowBody = str_replace('#ROWS#',$sbRowItems,$sbRowBody);
			
			$Res->success = 'true';
			$Res->errormessage = '';
			$Res->data = $getReplaceRowBody;					
			$Res->rowsperpage = (int)$RowsPerPage;
			$Res->pagenum = (int)$PageNum;
			$Res->maxrow = $MaxRow;
			$Res->norow = false;
			
		}else
		{
			$getReplaceRowBody = str_replace('#ROWS#',$RowEmpty,$sbRowBody);
			
			$Res->success = 'true';
			$Res->errormessage = '';
			$Res->data = $getReplaceRowBody;
			$Res->rowsperpage = (int)$RowsPerPage;
			$Res->pagenum = (int)$PageNum;
			$Res->maxrow = 0;
			$Res->norow = true;
			
		}

		return response()->json($Res);
		
	}
	
	public function insert()
	{
		$Res = new InsertUpdate();
		
		$sbRowBody = File::get(storage_path('common/rowsbody.txt'));
		$RowItemTemplate = view('module.usersys.row');
		$sbRowItems ='';
		$getReplaceRowBody = '';
		
		$user_id = Input::get('user_id');
		$user_name = Input::get('user_name');
		$password = Input::get('password');
		$user_type = Input::get('user_type');
		$user_type_var = Input::get('user_type_var');
		$email = Input::get('email');
		$phone = Input::get('phone');
		$isactive = Input::get('isactive');
		$create_by = Input::get('create_by');
		$create_byfn = Input::get('create_byfn');
		
		//$new_uid = Uuid::uuid4();
		$usersys = new UserSys;
		$usersys->user_id = $user_id;
		$usersys->user_name = $user_name;		
		$usersys->password = $password;
		$usersys->user_type = $user_type;
		$usersys->email = $email;
		$usersys->phone = $phone;
		$usersys->isactive = $isactive;
		$usersys->create_by = $create_by;
		$usersys->update_by = $create_by;
		
		$saved = $usersys->save();
		
		$create_at = $usersys->create_at;
		
		if($saved)
		{
			
			$usersys_user = UserSys::where('user_id','=', $create_by)->first();	
			$create_byfn = $usersys_user->user_name;
			
			$MaxRow = UserSys::count();
			$isactive_var;
			if($isactive == 1){$isactive_var = 'Aktif';}else{$isactive_var = 'Tidak Aktif';} 
			
			$sbRowItems = $sbRowItems.$RowItemTemplate;	
			$sbRowItems = str_replace('#recnum#',$MaxRow,$sbRowItems);
			$sbRowItems = str_replace('#user_id#',$user_id,$sbRowItems);
			$sbRowItems = str_replace('#user_name#',$user_name,$sbRowItems);
			$sbRowItems = str_replace('#password#',$password,$sbRowItems);
			$sbRowItems = str_replace('#user_type_var#',$user_type_var,$sbRowItems);
			$sbRowItems = str_replace('#phone#',$phone,$sbRowItems);
			$sbRowItems = str_replace('#email#',$email,$sbRowItems);
			$sbRowItems = str_replace('#isactive#',$isactive_var,$sbRowItems);
			
			$sbRowItems = str_replace('#create_by#',$create_by,$sbRowItems);
			$sbRowItems = str_replace('#create_byfn#',$create_byfn,$sbRowItems);
			$sbRowItems = str_replace('#create_at#',date_format($create_at,"Y-m-d"),$sbRowItems);
			$sbRowItems = str_replace('#update_by#',$create_by,$sbRowItems);
			$sbRowItems = str_replace('#update_byfn#',$create_byfn,$sbRowItems);
			$sbRowItems = str_replace('#update_at#',date_format($create_at,"Y-m-d"),$sbRowItems);
			
			$getReplaceRowBody = str_replace('#ROWS#',$sbRowItems,$sbRowBody);
			$HelperCustom = new HelperCustom();
			$ConvertCol = $HelperCustom->ConvertXmlColToArray($getReplaceRowBody);
			
			$Res->data = (string)$ConvertCol;				
			$Res->ud_user_id = $user_id;		
			$Res->ud_user_name = $user_name;		
			$Res->ud_password = $password;
			$Res->ud_user_type = $user_type;
			$Res->ud_email = $email;
			$Res->ud_phone = $phone;
			$Res->ud_isactive = $isactive;
			
			$Res->ud_create_by = $create_by;
			$Res->ud_create_byfn = $create_byfn;
			$Res->ud_create_at = $create_at;
			$Res->ud_update_by = $create_by;
			$Res->ud_update_byfn = $create_byfn;
			$Res->ud_update_at = $create_at;
			
			$Res->success = 'true';
			$Res->errormessege = '';
			$Res->recnum = $MaxRow;
		}else
		{
			$Res->success = 'false';
			$Res->errormessege = 'Error Insert';
		}
		
		return response()->json($Res);
		
	}	
	
	public function update()
	{
		$Res = new InsertUpdate();
		
		$sbRowBody = File::get(storage_path('common/rowsbody.txt'));
		$RowItemTemplate = view('module.usersys.rowNew');
		$sbRowItems ='';
		$getReplaceRowBody = '';
		
		$user_id = Input::get('user_id');
		$user_name = Input::get('user_name');
		$password = Input::get('password');
		$user_type = Input::get('user_type');
		$user_type_var = Input::get('user_type_var');
		$email = Input::get('email');
		$phone = Input::get('phone');
		$isactive = Input::get('isactive');
		
		$create_by = Input::get('create_by');
		$create_byfn = Input::get('create_byfn');
		$update_by = Input::get('update_by');
		$update_byfn = Input::get('update_byfn');
		
		$rowidx = Input::get('rowidx');

		$usersys = UserSys::where('user_id','=', $user_id)->first();		
		$usersys->user_name = $user_name;		
		$usersys->password = $password;
		$usersys->user_type = $user_type;
		$usersys->email = $email;
		$usersys->phone = $phone;
		$usersys->isactive = $isactive;
		
		$usersys->update_by = $update_by;
		
		$saved = $usersys->save();
		
		$create_at = $usersys->create_at;
		$update_at = $usersys->update_at;
		
		
		if($saved)
		{
			$usersys_user = UserSys::where('user_id','=', $update_by)->first();	
			$update_byfn = $usersys_user->user_name;
			
			$isactive_var;
			if($isactive == 1){$isactive_var = 'Aktif';}else{$isactive_var = 'Tidak Aktif';} 
			
			$sbRowItems = $sbRowItems.$RowItemTemplate;	
			$sbRowItems = str_replace('#recnum#',$rowidx,$sbRowItems);
			$sbRowItems = str_replace('#user_id#',$user_id,$sbRowItems);
			$sbRowItems = str_replace('#user_name#',$user_name,$sbRowItems);
			$sbRowItems = str_replace('#password#',$password,$sbRowItems);
			$sbRowItems = str_replace('#user_type_var#',$user_type_var,$sbRowItems);
			$sbRowItems = str_replace('#phone#',$phone,$sbRowItems);
			$sbRowItems = str_replace('#email#',$email,$sbRowItems);
			$sbRowItems = str_replace('#isactive#',$isactive_var,$sbRowItems);
			
			$sbRowItems = str_replace('#create_by#',$create_by,$sbRowItems);
			$sbRowItems = str_replace('#create_byfn#',$create_byfn,$sbRowItems);
			$sbRowItems = str_replace('#create_at#',date_format(new DateTime($create_at),"Y-m-d"),$sbRowItems);
			$sbRowItems = str_replace('#update_by#',$update_by,$sbRowItems);
			$sbRowItems = str_replace('#update_byfn#',$update_byfn,$sbRowItems);
			$sbRowItems = str_replace('#update_at#',date_format($update_at,"Y-m-d"),$sbRowItems);
			
			
			$getReplaceRowBody = str_replace('#ROWS#',$sbRowItems,$sbRowBody);
			$HelperCustom = new HelperCustom();
			$ConvertCol = $HelperCustom->ConvertXmlColToArray($getReplaceRowBody);
			
			$Res->data = (string)$ConvertCol;			
			$Res->ud_user_id = $user_id;		
			$Res->ud_user_name = $user_name;		
			$Res->ud_password = $password;
			$Res->ud_user_type = $user_type;
			$Res->ud_email = $email;
			$Res->ud_phone = $phone;
			$Res->ud_isactive = $isactive;
			
			$Res->ud_create_by = $create_by;
			$Res->ud_create_byfn = $create_byfn;
			$Res->ud_create_at = $create_at;
			$Res->ud_update_by = $create_by;
			$Res->ud_update_byfn = $create_byfn;
			$Res->ud_update_at = $create_at;
			
			$Res->success = 'true';
			$Res->errormessege = '';
			$Res->recnum=$rowidx;
			
		}else
		{
			$Res->success = 'false';
			$Res->errormessege = 'Error Insert';
		}
				
		return response()->json($Res);
		
	}	
	
	public function delete()
	{
		$Res = new DeleteResult();
		$user_id = explode(',', Input::get('user_id'));
		$tags = array();
		foreach ($user_id as $key=>$value){
			$tags[$key]=$value;
		}
		$usersys = UserSys::whereIn('user_id', $tags)->delete();	
		if($usersys)
		{
			$Res->success = 'true';
			$Res->errormessege = '';
			$Res->deletedid = $user_id;
		}else
		{
			$Res->success = 'false';
			$Res->errormessege = 'Error delete';
			$Res->deletedid = $user_id;
		}
		return response()->json($Res);
		
	}
	
	
	
}
