<?php

namespace App\Http\Controllers;
use File;
use Session;
use App\Classes\HelperCustom;
use App\Models\Roles;
use App\Models\RolesSubmodule;
use App\Models\UserSys;
use Ramsey\Uuid\Uuid;
use DateTime;

use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Contracts\Routing\ResponseFactory;

class Result
{
	public $success;
	public $errormessage;
	public $url;
	public $data;
	public $rowsperpage = 0; 	
	public $pagenum=0;
	public $maxrow=0;
	public $norow=false;
	public $selectopt;

}

class InsertUpdate
{
	public $success;
	public $errormessage;
	public $data;
	public $recnum;
	
	public $ud_roles_uid;	
	public $ud_roles_id;		
	public $ud_roles_name;
    public $ud_description;
	public $ud_isactive;
	
	public $ud_create_by;
	public $ud_create_byfn;
	public $ud_create_at;
	public $ud_update_by;
	public $ud_update_byfn;
	public $ud_update_at;
	
}

class InsertUpdateChld
{
	public $success;
	public $errormessage;
	
	public $ud_roles_uid;	
	public $ud_submodule_uid;		
	public $ud_update_by;
	public $ud_update_byfn;
	public $ud_update_at;
	
}

class DeleteResult
{
	public $success;
	public $errormessage;
	public $deletedid;
}



class RolesController extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

	public static function SetRolesSub($isselected,$roles_submodule,$submodule,$fields)
	{
		$Res ="";
		$ResId = $roles_submodule."|".$submodule;
		$Res = "<img id='".$ResId."' src=\"images/dhtmlxGrid/item_chk0.gif\" style='cursor:pointer;' onclick=\"SubmitRolesSubmodule('".$roles_submodule."','".$submodule."','".$fields."')\">";
		if ($isselected > 0) $Res = "<img id='".$ResId."' src=\"images/dhtmlxGrid/item_chk1.gif\" style='cursor:pointer;' onclick=\"SubmitRolesSubmodule('".$roles_submodule."','".$submodule."','".$fields."')\">";

		return $Res;

	}
	
	public function index()
	{
		
		return view('module.roles.index',[
			'js' => 'mootools',
        ]);
		
	}
	
	public function show()
	{
		$Res = new Result();
		$RowsPerPage = Input::get('rowsperpage');
		$PageNum = Input::get('pagenum');
		$CurentNav = Input::get('curentnav');
		$search = Input::get('search');
		$MaxRow = 0; 
		$offset = ($PageNum-1)*$RowsPerPage;
		
		// prepare file base variable
		$sbRowBody = File::get(storage_path('common/rowsbody.txt'));
		$sbRowItems ='';
		$getReplaceRowBody = '';
		$RowItemTemplate = view('module.roles.row');
		$RowEmpty = view('module.roles.rowEmpty');
				
		$Roles = Roles::getByPage($RowsPerPage,$offset,$search);
		
		foreach($Roles->maxrow as $key=>$value)
		{
			$MaxRow = $value['maxrow'];
		}
		if(!empty($Roles->selectrow))
		{
			foreach($Roles->selectrow as $rol)
			{
				
				$isactive;
				if($rol['isactive'] == 'Aktif'){$isactive = 1;}else{$isactive = 0;} 
				
				$sbRowItems = $sbRowItems.$RowItemTemplate;
				$sbRowItems = str_replace('#ud_role_uid#',$rol['role_uid'],$sbRowItems);
				$sbRowItems = str_replace('#ud_role_id#',$rol['role_id'],$sbRowItems);
				$sbRowItems = str_replace('#ud_role_name#',$rol['role_name'],$sbRowItems);
				$sbRowItems = str_replace('#ud_description#',$rol['description'],$sbRowItems);
				$sbRowItems = str_replace('#ud_isactive#',$isactive,$sbRowItems);
				
				$sbRowItems = str_replace('#ud_create_by#',$rol['create_by'],$sbRowItems);
				$sbRowItems = str_replace('#ud_create_byfn#',$rol['create_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#ud_create_at#',$rol['create_at'],$sbRowItems);
				$sbRowItems = str_replace('#ud_update_by#',$rol['update_by'],$sbRowItems);
				$sbRowItems = str_replace('#ud_update_byfn#',$rol['update_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#ud_update_at#',$rol['update_at'],$sbRowItems);

				
				$sbRowItems = str_replace('#recnum#',$rol['recnum'],$sbRowItems);
				$sbRowItems = str_replace('#role_uid#',$rol['role_uid'],$sbRowItems);
				$sbRowItems = str_replace('#role_id#',$rol['role_id'],$sbRowItems);
				$sbRowItems = str_replace('#role_name#',$rol['role_name'],$sbRowItems);
				$sbRowItems = str_replace('#description#',$rol['description'],$sbRowItems);
				$sbRowItems = str_replace('#isactive#',$rol['isactive'],$sbRowItems);
				
				$sbRowItems = str_replace('#create_by#',$rol['create_by'],$sbRowItems);
				$sbRowItems = str_replace('#create_byfn#',$rol['create_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#create_at#',$rol['create_at'],$sbRowItems);
				$sbRowItems = str_replace('#update_by#',$rol['update_by'],$sbRowItems);
				$sbRowItems = str_replace('#update_byfn#',$rol['update_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#update_at#',$rol['update_at'],$sbRowItems);
				
			}
			
			$getReplaceRowBody = str_replace('#ROWS#',$sbRowItems,$sbRowBody);
			
			$Res->success = 'true';
			$Res->errormessage = '';
			$Res->data = $getReplaceRowBody;					
			$Res->rowsperpage = (int)$RowsPerPage;
			$Res->pagenum = (int)$PageNum;
			$Res->maxrow = $MaxRow;
			$Res->norow = false;
			
		}else
		{
			$getReplaceRowBody = str_replace('#ROWS#',$RowEmpty,$sbRowBody);
			
			$Res->success = 'true';
			$Res->errormessage = '';
			$Res->data = $getReplaceRowBody;
			$Res->rowsperpage = (int)$RowsPerPage;
			$Res->pagenum = (int)$PageNum;
			$Res->maxrow = 0;
			$Res->norow = true;
			
		}

		return response()->json($Res);
		
	}
	
	public function insert()
	{
		$Res = new InsertUpdate();
		
		$sbRowBody = File::get(storage_path('common/rowsbody.txt'));
		$RowItemTemplate = view('module.roles.row');
		$sbRowItems ='';
		$getReplaceRowBody = '';
		
		$role_id = Input::get('role_id');
		$role_name = Input::get('role_name');
		$description = Input::get('description');
		$isactive = Input::get('isactive');
		$create_by = Input::get('create_by');
		$create_byfn = Input::get('create_byfn');
		
		$new_uid = Uuid::uuid4();
		$Roles = new Roles;
		$Roles->role_uid = $new_uid;
		$Roles->role_id = $role_id;
		$Roles->role_name = $role_name;		
		$Roles->description = $description;
		$Roles->isactive = $isactive;
		$Roles->create_by = $create_by;
		$Roles->update_by = $create_by;
		
		$saved = $Roles->save();
		
		$create_at = $Roles->create_at;
		
		if($saved)
		{
			
			$usersys = UserSys::where('user_id','=', $create_by)->first();	
			$create_byfn = $usersys->user_name;
			
			$MaxRow = Roles::count();
			$isactive_var;
			if($isactive == 1){$isactive_var = 'Aktif';}else{$isactive_var = 'Tidak Aktif';} 
			
			$sbRowItems = $sbRowItems.$RowItemTemplate;	
			$sbRowItems = str_replace('#recnum#',$MaxRow,$sbRowItems);
			$sbRowItems = str_replace('#role_uid#',$new_uid,$sbRowItems);
			$sbRowItems = str_replace('#role_id#',$role_id,$sbRowItems);
			$sbRowItems = str_replace('#role_name#',$role_name,$sbRowItems);
			$sbRowItems = str_replace('#description#',$description,$sbRowItems);
			$sbRowItems = str_replace('#isactive#',$isactive_var,$sbRowItems);
			
			$sbRowItems = str_replace('#create_by#',$create_by,$sbRowItems);
			$sbRowItems = str_replace('#create_byfn#',$create_byfn,$sbRowItems);
			$sbRowItems = str_replace('#create_at#',date_format($create_at,"Y-m-d"),$sbRowItems);
			$sbRowItems = str_replace('#update_by#',$create_by,$sbRowItems);
			$sbRowItems = str_replace('#update_byfn#',$create_byfn,$sbRowItems);
			$sbRowItems = str_replace('#update_at#',date_format($create_at,"Y-m-d"),$sbRowItems);
			
			$getReplaceRowBody = str_replace('#ROWS#',$sbRowItems,$sbRowBody);
			$HelperCustom = new HelperCustom();
			$ConvertCol = $HelperCustom->ConvertXmlColToArray($getReplaceRowBody);
			
			$Res->data = (string)$ConvertCol;	
			$Res->ud_role_uid = $new_uid;	
			$Res->ud_role_id = $role_id;		
			$Res->ud_role_name = $role_name;		
			$Res->ud_description = $description;
			$Res->ud_isactive = $isactive;
			
			$Res->ud_create_by = $create_by;
			$Res->ud_create_byfn = $create_byfn;
			$Res->ud_create_at = $create_at;
			$Res->ud_update_by = $create_by;
			$Res->ud_update_byfn = $create_byfn;
			$Res->ud_update_at = $create_at;
			
			$Res->success = 'true';
			$Res->errormessege = '';
			$Res->recnum = $MaxRow;
		}else
		{
			$Res->success = 'false';
			$Res->errormessege = 'Error Insert';
		}
		
		return response()->json($Res);
		
	}	
	
	public function update()
	{
		$Res = new InsertUpdate();
		
		$sbRowBody = File::get(storage_path('common/rowsbody.txt'));
		$RowItemTemplate = view('module.roles.rowNew');
		$sbRowItems ='';
		$getReplaceRowBody = '';
		
		$role_uid = Input::get('role_uid');
		$role_id = Input::get('role_id');
		$role_name = Input::get('role_name');
		$description = Input::get('description');
		$isactive = Input::get('isactive');
		
		$create_by = Input::get('create_by');
		$create_byfn = Input::get('create_byfn');
		$update_by = Input::get('update_by');
		$update_byfn = Input::get('update_byfn');
		
		$rowidx = Input::get('rowidx');

		$Roles = Roles::where('role_uid','=', $role_uid)->first();
		$Roles->role_id = $role_id;	
		$Roles->role_name = $role_name;		
		$Roles->description = $description;
		$Roles->isactive = $isactive;
		
		$Roles->update_by = $update_by;
		
		$saved = $Roles->save();
		
		$create_at = $Roles->create_at;
		$update_at = $Roles->update_at;
		
		
		if($saved)
		{
			$usersys = UserSys::where('user_id','=', $update_by)->first();	
			$update_byfn = $usersys->user_name;
			
			$isactive_var;
			if($isactive == 1){$isactive_var = 'Aktif';}else{$isactive_var = 'Tidak Aktif';} 
			
			$sbRowItems = $sbRowItems.$RowItemTemplate;	
			$sbRowItems = str_replace('#recnum#',$rowidx,$sbRowItems);
			$sbRowItems = str_replace('#role_uid#',$role_uid,$sbRowItems);
			$sbRowItems = str_replace('#role_id#',$role_id,$sbRowItems);
			$sbRowItems = str_replace('#role_name#',$role_name,$sbRowItems);
			$sbRowItems = str_replace('#description#',$description,$sbRowItems);
			$sbRowItems = str_replace('#isactive#',$isactive_var,$sbRowItems);
			
			$sbRowItems = str_replace('#create_by#',$create_by,$sbRowItems);
			$sbRowItems = str_replace('#create_byfn#',$create_byfn,$sbRowItems);
			$sbRowItems = str_replace('#create_at#',date_format(new DateTime($create_at),"Y-m-d"),$sbRowItems);
			$sbRowItems = str_replace('#update_by#',$update_by,$sbRowItems);
			$sbRowItems = str_replace('#update_byfn#',$update_byfn,$sbRowItems);
			$sbRowItems = str_replace('#update_at#',date_format($update_at,"Y-m-d"),$sbRowItems);
			
			
			$getReplaceRowBody = str_replace('#ROWS#',$sbRowItems,$sbRowBody);
			$HelperCustom = new HelperCustom();
			$ConvertCol = $HelperCustom->ConvertXmlColToArray($getReplaceRowBody);
			
			$Res->data = (string)$ConvertCol;
			$Res->ud_role_uid = $role_uid;
			$Res->ud_role_id = $role_id;		
			$Res->ud_role_name = $role_name;		
			$Res->ud_description = $description;
			$Res->ud_isactive = $isactive;
			
			$Res->ud_create_by = $create_by;
			$Res->ud_create_byfn = $create_byfn;
			$Res->ud_create_at = $create_at;
			$Res->ud_update_by = $create_by;
			$Res->ud_update_byfn = $create_byfn;
			$Res->ud_update_at = $create_at;
			
			$Res->success = 'true';
			$Res->errormessege = '';
			$Res->recnum=$rowidx;
			
		}else
		{
			$Res->success = 'false';
			$Res->errormessege = 'Error Insert';
		}
				
		return response()->json($Res);
		
	}	
	
	public function delete()
	{
		$Res = new DeleteResult();
		$role_uid = explode(',', Input::get('role_uid'));
		$tags = array();
		foreach ($role_uid as $key=>$value){
			$tags[$key]=$value;
		}
		$Roles = Roles::whereIn('role_uid', $tags)->delete();	
		if($Roles)
		{
			$Res->success = 'true';
			$Res->errormessege = '';
			$Res->deletedid = $role_uid;
		}else
		{
			$Res->success = 'false';
			$Res->errormessege = 'Error delete';
			$Res->deletedid = $role_uid;
		}
		return response()->json($Res);
		
	}
	
	public function indexSub()
	{
		
		return view('module.roles.rolemodule',[
			'js' => 'mootools',
        ]);
		
	}
	
	public function showMod()
	{
		
		$Res = new Result();
		$RowsPerPage = Input::get('rowsperpage');
		$PageNum = Input::get('pagenum');
		$CurentNav = Input::get('curentnav');
		$search = Input::get('search');
		$role_uid = Input::get('role_uid');
		$MaxRow = 0; 
		$offset = ($PageNum-1)*$RowsPerPage;
		
		// prepare file base variable
		$sbRowBody = File::get(storage_path('common/rowsbody.txt'));
		$sbRowItems ='';
		$getReplaceRowBody = '';
		$RowItemTemplate = view('module.roles.row2');
		$RowEmpty = view('module.roles.rowEmpty2');
				
		$Roles = RolesSubmodule::getByPageSubModule($RowsPerPage,$offset,$search,$role_uid);
		
		foreach($Roles->maxrow as $key=>$value)
		{
			$MaxRow = $value['maxrow'];
		}
		if(!empty($Roles->selectrow))
		{
			foreach($Roles->selectrow as $rol)
			{
				
				//$isactive;
				//if($rol['isactive'] == 'Aktif'){$isactive = 1;}else{$isactive = 0;} 
				
				$sbRowItems = $sbRowItems.$RowItemTemplate;
				$sbRowItems = str_replace('#ud_submodule_uid#',$rol['submodule_uid'],$sbRowItems);
				$sbRowItems = str_replace('#ud_role_uid#',$role_uid,$sbRowItems);
				$sbRowItems = str_replace('#ud_submodule_id#',$rol['submodule_id'],$sbRowItems);
				$sbRowItems = str_replace('#ud_submodule_name#',$rol['submodule_name'],$sbRowItems);	
				$sbRowItems = str_replace('#ud_description#',$rol['description'],$sbRowItems);
				$sbRowItems = str_replace('#ud_isselected#',$rol['isselected'],$sbRowItems);
				$sbRowItems = str_replace('#ud_editable#',$rol['editable'],$sbRowItems);
				$sbRowItems = str_replace('#ud_addable#',$rol['addable'],$sbRowItems);
				$sbRowItems = str_replace('#ud_deleteable#',$rol['deleteable'],$sbRowItems);
				
				//$sbRowItems = str_replace('#ud_create_by#',$rol['create_by'],$sbRowItems);
				//$sbRowItems = str_replace('#ud_create_byfn#',$rol['create_byfn'],$sbRowItems);
				//$sbRowItems = str_replace('#ud_create_at#',$rol['create_at'],$sbRowItems);
				$sbRowItems = str_replace('#ud_update_by#',$rol['update_by'],$sbRowItems);
				$sbRowItems = str_replace('#ud_update_byfn#',$rol['update_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#ud_update_at#',$rol['update_at'],$sbRowItems);

				
				$sbRowItems = str_replace('#recnum#',$rol['recnum'],$sbRowItems);
				$sbRowItems = str_replace('#isselected#',$this::SetRolesSub($rol['isselected'],$role_uid,$rol['submodule_uid'],'0'),$sbRowItems);
				$sbRowItems = str_replace('#submodule_uid#',$rol['submodule_uid'],$sbRowItems);
				$sbRowItems = str_replace('#submodule_name#',$rol['submodule_name'],$sbRowItems);
				if($rol['isselected'] == 1){
					$sbRowItems = str_replace('#addable#',$this::SetRolesSub($rol['addable'],$role_uid,$rol['submodule_uid'],'1'),$sbRowItems);
					$sbRowItems = str_replace('#editable#',$this::SetRolesSub($rol['editable'],$role_uid,$rol['submodule_uid'],'2'),$sbRowItems);
					$sbRowItems = str_replace('#deleteable#',$this::SetRolesSub($rol['deleteable'],$role_uid,$rol['submodule_uid'],'3'),$sbRowItems);
				}else
				{
					$sbRowItems = str_replace('#editable#','',$sbRowItems);
					$sbRowItems = str_replace('#addable#','',$sbRowItems);
					$sbRowItems = str_replace('#deleteable#','',$sbRowItems);
				}
				
				
				//$sbRowItems = str_replace('#create_by#',$rol['create_by'],$sbRowItems);
				//$sbRowItems = str_replace('#create_byfn#',$rol['create_byfn'],$sbRowItems);
				//$sbRowItems = str_replace('#create_at#',$rol['create_at'],$sbRowItems);
				$sbRowItems = str_replace('#update_by#',$rol['update_by'],$sbRowItems); 
				$sbRowItems = str_replace('#update_byfn#',$rol['update_byfn'],$sbRowItems);
				//$sbRowItems = str_replace('#update_at#',date_format($rol['update_at'],"Y-m-d"),$sbRowItems);
				$sbRowItems = str_replace('#update_at#',$rol['update_at'],$sbRowItems);
				
			}
			
			$getReplaceRowBody = str_replace('#ROWS#',$sbRowItems,$sbRowBody);
			
			$Res->success = 'true';
			$Res->errormessage = '';
			$Res->data = $getReplaceRowBody;					
			$Res->rowsperpage = (int)$RowsPerPage;
			$Res->pagenum = (int)$PageNum;
			$Res->maxrow = $MaxRow;
			$Res->norow = false;
			
		}else
		{
			$getReplaceRowBody = str_replace('#ROWS#',$RowEmpty,$sbRowBody);
			
			$Res->success = 'true';
			$Res->errormessage = '';
			$Res->data = $getReplaceRowBody;
			$Res->rowsperpage = (int)$RowsPerPage;
			$Res->pagenum = (int)$PageNum;
			$Res->maxrow = 0;
			$Res->norow = true;
			
		}

		return response()->json($Res);
		
	}
	
	public function insertChld()
	{
		$Res = new InsertUpdateChld();
		
		$role_uid = Input::get('role_uid');
		$submodule_uid = Input::get('submodule_uid');
		$create_by = Input::get('create_by');
		$create_byfn = Input::get('create_byfn');
		
		$RolesSubmodule = new RolesSubmodule;
		$RolesSubmodule->role_uid = $role_uid;
		$RolesSubmodule->submodule_uid = $submodule_uid;
		$RolesSubmodule->update_by = $create_by;
		
		$saved = $RolesSubmodule->save();
		
		//$create_at = $RolesSubmodule->create_at;
		
		if($saved)
		{
			
			//$usersys = UserSys::where('user_id','=', $create_by)->first();	
			//$create_byfn = $usersys->user_name;
			
			//$Res->ud_role_uid = $new_uid;	
			//$Res->ud_submodule_uid = $submodule_uid;
			
			//$Res->ud_update_by = $create_by;
			//$Res->ud_update_byfn = $create_byfn;
			//$Res->ud_update_at = $create_at;
			
			$Res->success = 'true';
			$Res->errormessege = '';
		}else
		{
			$Res->success = 'false';
			$Res->errormessege = 'Error Insert';
		}
		
		return response()->json($Res);
		
	}	
	
	public function deleteChld()
	{
		$Res = new DeleteResult();
		$role_uid = Input::get('role_uid');
		$submodule_uid = Input::get('submodule_uid');
		$RolesSubmodule = RolesSubmodule::where('role_uid', $role_uid)->where('submodule_uid',$submodule_uid)->delete();	
		if($RolesSubmodule)
		{
			$Res->success = 'true';
			$Res->errormessege = '';
			$Res->deletedid = $role_uid.'|'.$submodule_uid;
		}else
		{
			$Res->success = 'false';
			$Res->errormessege = 'Error delete';
			$Res->deletedid = $role_uid.'|'.$submodule_uid;
		}
		return response()->json($Res);
		
	}
	
	public function updateChld()
	{
		$Res = new InsertUpdateChld();

		$role_uid = Input::get('role_uid');
		$submodule_uid = Input::get('submodule_uid');
		$fields = Input::get('fields');
		$valueselected = Input::get('valueselected');
		$update_by = Input::get('update_by');
		$update_byfn = Input::get('update_byfn');
		$RolesSubmodule = RolesSubmodule::where('role_uid','=', $role_uid)->where('submodule_uid','=',$submodule_uid)->first();
		
		if($fields=='1')
		{
			$RolesSubmodule->addable = $valueselected;
		}else if($fields =='2')
		{
			$RolesSubmodule->editable = $valueselected;
		}else{
			$RolesSubmodule->deleteable = $valueselected;
		}
		$RolesSubmodule->update_by = $update_by;
		
		$saved = $RolesSubmodule->save();
		if($saved)
		{
			$Res->success = 'true';
			$Res->errormessege = '';
			
		}else
		{
			$Res->success = 'false';
			$Res->errormessege = 'Error Insert';
		}
				
		return response()->json($Res);
		
	}	
	
	
	
	
	
}
