<?php

namespace App\Http\Controllers;
use File;
use Session;
use App\Classes\HelperCustom;
use App\Models\Comp;
use Ramsey\Uuid\Uuid;

use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Contracts\Routing\ResponseFactory;

class Result
{
	public $success;
	public $errormessage;
	public $url;
	public $data;
	public $rowsperpage = 0; 	
	public $pagenum=0;
	public $maxrow=0;
	public $norow=false;
	public $selectopt;

}

class InsertUpdate
{
	public $success;
	public $errormessage;
	public $data;
	public $recnum;
	
	public $ud_comp_uid;	
	public $ud_comp_id;		
	public $ud_comp_name;
	public $ud_comp_address;
	public $ud_comp_propinsi;
	public $ud_comp_kabupaten;
	public $ud_comp_kecamatan;
	public $ud_comp_kelurahan;
	public $ud_comp_email;
	public $ud_comp_telp;
	public $ud_isactive;
	public $ud_create_by;
	public $ud_create_byfn;
	public $ud_create_at;
	public $ud_update_by;
	public $ud_update_byfn;
	public $ud_update_at;
	
}

class DeleteResult
{
	public $success;
	public $errormessage;
	public $deletedid;
}	

class CompController extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
	
	public function index()
	{
		
		return view('module.comp.index',[
			'js' => 'mootools',
        ]);
		
	}
	
	public function show()
	{
		$Res = new Result();
		$RowsPerPage = Input::get('rowsperpage');
		$PageNum = Input::get('pagenum');
		$CurentNav = Input::get('curentnav');
		$search = Input::get('search');
		$MaxRow = 0; 
		$offset = ($PageNum-1)*$RowsPerPage;
		
		// prepare file base variable
		$sbRowBody = File::get(storage_path('common/rowsbody.txt'));
		$sbRowItems ='';
		$getReplaceRowBody = '';
		$RowItemTemplate = view('module.comp.row');
		$RowEmpty = view('module.comp.rowEmpty');
				
		$comp = Comp::getByPage($RowsPerPage,$offset,$search);
		
		foreach($comp->maxrow as $key=>$value)
		{
			$MaxRow = $value['maxrow'];
		}
		if(!empty($comp->selectrow))
		{
			foreach($comp->selectrow as $comps)
			{
				
				$isactive;
				if($comps['isactive'] == 'Aktif'){$isactive = 1;}else{$isactive = 0;} 
				
				$sbRowItems = $sbRowItems.$RowItemTemplate;
				$sbRowItems = str_replace('#ud_comp_uid#',$comps['comp_uid'],$sbRowItems);
				$sbRowItems = str_replace('#ud_comp_id#',$comps['comp_id'],$sbRowItems);
				$sbRowItems = str_replace('#ud_comp_name#',$comps['comp_name'],$sbRowItems);
				$sbRowItems = str_replace('#ud_comp_address#',$comps['comp_address'],$sbRowItems);
				$sbRowItems = str_replace('#ud_comp_propinsi#',$comps['comp_propinsi'],$sbRowItems);
				$sbRowItems = str_replace('#ud_comp_kabupaten#',$comps['comp_kabupaten'],$sbRowItems);
				$sbRowItems = str_replace('#ud_comp_kecamatan#',$comps['comp_kecamatan'],$sbRowItems);
				$sbRowItems = str_replace('#ud_comp_kelurahan#',$comps['comp_kelurahan'],$sbRowItems);
				$sbRowItems = str_replace('#ud_comp_email#',$comps['comp_email'],$sbRowItems);
				$sbRowItems = str_replace('#ud_comp_telp#',$comps['comp_telp'],$sbRowItems);
				$sbRowItems = str_replace('#ud_isactive#',$isactive,$sbRowItems);
				$sbRowItems = str_replace('#ud_create_by#',$comps['create_by'],$sbRowItems);
				$sbRowItems = str_replace('#ud_create_byfn#',$comps['create_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#ud_create_at#',$comps['create_at'],$sbRowItems);
				$sbRowItems = str_replace('#ud_update_by#',$comps['update_by'],$sbRowItems);
				$sbRowItems = str_replace('#ud_update_byfn#',$comps['update_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#ud_update_at#',$comps['update_at'],$sbRowItems);

				
				$sbRowItems = str_replace('#recnum#',$comps['recnum'],$sbRowItems);
				$sbRowItems = str_replace('#comp_uid#',$comps['comp_uid'],$sbRowItems);
				$sbRowItems = str_replace('#comp_id#',$comps['comp_id'],$sbRowItems);
				$sbRowItems = str_replace('#comp_name#',$comps['comp_name'],$sbRowItems);
				$sbRowItems = str_replace('#comp_address#',$comps['comp_address'],$sbRowItems);
				$sbRowItems = str_replace('#comp_propinsi#',$comps['comp_propinsi'],$sbRowItems);
				$sbRowItems = str_replace('#comp_kabupaten#',$comps['comp_kabupaten'],$sbRowItems);
				$sbRowItems = str_replace('#comp_kecamatan#',$comps['comp_kecamatan'],$sbRowItems);
				$sbRowItems = str_replace('#comp_kelurahan#',$comps['comp_kelurahan'],$sbRowItems);
				$sbRowItems = str_replace('#comp_email#',$comps['comp_email'],$sbRowItems);
				$sbRowItems = str_replace('#comp_telp#',$comps['comp_telp'],$sbRowItems);
				$sbRowItems = str_replace('#isactive#',$comps['isactive'],$sbRowItems);
				$sbRowItems = str_replace('#create_by#',$comps['create_by'],$sbRowItems);
				$sbRowItems = str_replace('#create_byfn#',$comps['create_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#create_at#',$comps['create_at'],$sbRowItems);
				$sbRowItems = str_replace('#update_by#',$comps['update_by'],$sbRowItems);
				$sbRowItems = str_replace('#update_byfn#',$comps['update_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#update_at#',$comps['update_at'],$sbRowItems);
				
			}
			
			$getReplaceRowBody = str_replace('#ROWS#',$sbRowItems,$sbRowBody);
			
			$Res->success = 'true';
			$Res->errormessage = '';
			$Res->data = $getReplaceRowBody;					
			$Res->rowsperpage = (int)$RowsPerPage;
			$Res->pagenum = (int)$PageNum;
			$Res->maxrow = $MaxRow;
			$Res->norow = false;
			
		}else
		{
			$getReplaceRowBody = str_replace('#ROWS#',$RowEmpty,$sbRowBody);
			
			$Res->success = 'true';
			$Res->errormessage = '';
			$Res->data = $getReplaceRowBody;
			$Res->rowsperpage = (int)$RowsPerPage;
			$Res->pagenum = (int)$PageNum;
			$Res->maxrow = 0;
			$Res->norow = true;
			
		}

		return response()->json($Res);
		
	}
	
	public function insert()
	{
		$Res = new InsertUpdate();
		
		$sbRowBody = File::get(storage_path('common/rowsbody.txt'));
		$RowItemTemplate = view('module.comp.row');
		$sbRowItems ='';
		$getReplaceRowBody = '';
		
		$comp_id = Input::get('comp_id');
		$comp_name = Input::get('comp_name');
		$comp_address = Input::get('comp_address');
		$comp_propinsi = Input::get('comp_propinsi');
		$comp_kabupaten = Input::get('comp_kabupaten');
		$comp_kecamatan = Input::get('comp_kecamatan');
		$comp_kelurahan = Input::get('comp_kelurahan');
		$comp_email = Input::get('comp_email');
		$comp_telp = Input::get('comp_telp');
		$isactive = Input::get('isactive');
		$create_by = Input::get('create_by');
		$create_byfn = Input::get('create_byfn');
		
		$new_uid = Uuid::uuid4();
		$comp = new Comp;
		$comp->comp_uid = $new_uid;
		$comp->comp_id = $comp_id;
		$comp->comp_name = $comp_name;		
		$comp->comp_address = $comp_address;
		$comp->comp_propinsi = $comp_propinsi;
		$comp->comp_kabupaten = $comp_kabupaten;
		$comp->comp_kecamatan = $comp_kecamatan;
		$comp->comp_kelurahan = $comp_kelurahan;
		$comp->comp_email = $comp_email;
		$comp->comp_telp = $comp_telp;
		$comp->isactive = $isactive;
		$comp->create_by = $create_by;
		$comp->update_by = $create_by;
		
		$saved = $comp->save();
		
		$create_at = $comp->create_at;
		
		if($saved)
		{
			$MaxRow = Comp::count();
			$isactive_var;
			if($isactive == 1){$isactive_var = 'Aktif';}else{$isactive_var = 'Tidak Aktif';} 
			
			$sbRowItems = $sbRowItems.$RowItemTemplate;	
			$sbRowItems = str_replace('#recnum#',$MaxRow,$sbRowItems);
			$sbRowItems = str_replace('#comp_uid#',$new_uid,$sbRowItems);
			$sbRowItems = str_replace('#comp_id#',$comp_id,$sbRowItems);
			$sbRowItems = str_replace('#comp_name#',$comp_name,$sbRowItems);
			$sbRowItems = str_replace('#comp_address#',$comp_address,$sbRowItems);
			$sbRowItems = str_replace('#comp_propinsi#',$comp_propinsi,$sbRowItems);
			$sbRowItems = str_replace('#comp_kabupaten#',$comp_kabupaten,$sbRowItems);
			$sbRowItems = str_replace('#comp_kecamatan#',$comp_kecamatan,$sbRowItems);
			$sbRowItems = str_replace('#comp_kelurahan#',$comp_kelurahan,$sbRowItems);
			$sbRowItems = str_replace('#comp_email#',$comp_email,$sbRowItems);
			$sbRowItems = str_replace('#comp_telp#',$comp_telp,$sbRowItems);
			$sbRowItems = str_replace('#isactive#',$isactive_var,$sbRowItems);
			
			$sbRowItems = str_replace('#create_by#',$create_by,$sbRowItems);
			$sbRowItems = str_replace('#create_byfn#',$create_byfn,$sbRowItems);
			$sbRowItems = str_replace('#create_at#',date_format($create_at,"Y-m-d"),$sbRowItems);
			$sbRowItems = str_replace('#update_by#',$create_by,$sbRowItems);
			$sbRowItems = str_replace('#update_byfn#',$create_byfn,$sbRowItems);
			$sbRowItems = str_replace('#update_at#',date_format($create_at,"Y-m-d"),$sbRowItems);
			
			$getReplaceRowBody = str_replace('#ROWS#',$sbRowItems,$sbRowBody);
			$HelperCustom = new HelperCustom();
			$ConvertCol = $HelperCustom->ConvertXmlColToArray($getReplaceRowBody);
			
			$Res->data = (string)$ConvertCol;
			$Res->ud_comp_uid = $new_uid;
			$Res->ud_comp_id = $comp_id;		
			$Res->ud_comp_name = $comp_name;		
			$Res->ud_comp_address = $comp_address;
			$Res->ud_comp_propinsi = $comp_propinsi;
			$Res->ud_comp_kabupaten = $comp_kabupaten;
			$Res->ud_comp_kecamatan = $comp_kecamatan;
			$Res->ud_comp_kelurahan = $comp_kelurahan;
			$Res->ud_comp_email = $comp_email;
			$Res->ud_comp_telp = $comp_telp;
			$Res->ud_isactive = $isactive;
			
			$Res->ud_create_by = $create_by;
			$Res->ud_create_byfn = $create_byfn;
			$Res->ud_create_at = $create_at;
			$Res->ud_update_by = $create_by;
			$Res->ud_update_byfn = $create_byfn;
			$Res->ud_update_at = $create_at;
			
			$Res->success = 'true';
			$Res->errormessege = '';
			$Res->recnum = $MaxRow;
		}else
		{
			$Res->success = 'false';
			$Res->errormessege = 'Error Insert';
		}
		
		return response()->json($Res);
		
	}	
	
	public function update()
	{
		$Res = new InsertUpdate();
		
		$sbRowBody = File::get(storage_path('common/rowsbody.txt'));
		$RowItemTemplate = view('module.comp.rowNew');
		$sbRowItems ='';
		$getReplaceRowBody = '';
		
		$comp_uid = Input::get('comp_uid');
		$comp_id = Input::get('comp_id');
		$comp_name = Input::get('comp_name');
		$comp_address = Input::get('comp_address');
		$comp_propinsi = Input::get('comp_propinsi');
		$comp_kabupaten = Input::get('comp_kabupaten');
		$comp_kecamatan = Input::get('comp_kecamatan');
		$comp_kelurahan = Input::get('comp_kelurahan');
		$comp_email = Input::get('comp_email');
		$comp_telp = Input::get('comp_telp');
		$isactive = Input::get('isactive');
		$create_by = Input::get('create_by');
		$create_byfn = Input::get('create_byfn');
		$update_by = Input::get('update_by');
		$update_byfn = Input::get('update_byfn');
		
		$rowidx = Input::get('rowidx');

		$comp = Comp::where('comp_uid','=', $comp_uid)->first();		
		$comp->comp_id = $comp_id;
		$comp->comp_name = $comp_name;		
		$comp->comp_address = $comp_address;
		$comp->comp_propinsi = $comp_propinsi;
		$comp->comp_kabupaten = $comp_kabupaten;
		$comp->comp_kecamatan = $comp_kecamatan;
		$comp->comp_kelurahan = $comp_kelurahan;
		$comp->comp_email = $comp_email;
		$comp->comp_telp = $comp_telp;
		$comp->isactive = $isactive;
		$comp->update_by = $update_by;
		$saved = $comp->save();
		
		$create_at = $comp->create_at;
		$update_at = $comp->update_at;
		
		if($saved)
		{
			
			$isactive_var;
			if($isactive == 1){$isactive_var = 'Aktif';}else{$isactive_var = 'Tidak Aktif';} 
			
			$sbRowItems = $sbRowItems.$RowItemTemplate;	
			$sbRowItems = str_replace('#recnum#',$rowidx,$sbRowItems);
			$sbRowItems = str_replace('#comp_uid#',$comp_uid,$sbRowItems);
			$sbRowItems = str_replace('#comp_id#',$comp_id,$sbRowItems);
			$sbRowItems = str_replace('#comp_name#',$comp_name,$sbRowItems);
			$sbRowItems = str_replace('#comp_address#',$comp_address,$sbRowItems);
			$sbRowItems = str_replace('#comp_propinsi#',$comp_propinsi,$sbRowItems);
			$sbRowItems = str_replace('#comp_kabupaten#',$comp_kabupaten,$sbRowItems);
			$sbRowItems = str_replace('#comp_kecamatan#',$comp_kecamatan,$sbRowItems);
			$sbRowItems = str_replace('#comp_kelurahan#',$comp_kelurahan,$sbRowItems);
			$sbRowItems = str_replace('#comp_email#',$comp_email,$sbRowItems);
			$sbRowItems = str_replace('#comp_telp#',$comp_telp,$sbRowItems);
			$sbRowItems = str_replace('#isactive#',$isactive_var,$sbRowItems);
			
			$sbRowItems = str_replace('#create_by#',$create_by,$sbRowItems);
			$sbRowItems = str_replace('#create_byfn#',$create_byfn,$sbRowItems);
			$sbRowItems = str_replace('#create_at#',date_format($create_at,"Y-m-d"),$sbRowItems);
			$sbRowItems = str_replace('#update_by#',$create_by,$sbRowItems);
			$sbRowItems = str_replace('#update_byfn#',$create_byfn,$sbRowItems);
			$sbRowItems = str_replace('#update_at#',date_format($create_at,"Y-m-d"),$sbRowItems);
			
			
			$getReplaceRowBody = str_replace('#ROWS#',$sbRowItems,$sbRowBody);
			$HelperCustom = new HelperCustom();
			$ConvertCol = $HelperCustom->ConvertXmlColToArray($getReplaceRowBody);
			
			$Res->data = (string)$ConvertCol;			
			$Res->ud_comp_uid = $comp_uid;		
			$Res->ud_comp_id = $comp_id;		
			$Res->ud_comp_name = $comp_name;		
			$Res->ud_comp_address = $comp_address;
			$Res->ud_comp_propinsi = $comp_propinsi;
			$Res->ud_comp_kabupaten = $comp_kabupaten;
			$Res->ud_comp_kecamatan = $comp_kecamatan;
			$Res->ud_comp_kelurahan = $comp_kelurahan;
			$Res->ud_comp_email = $comp_email;
			$Res->ud_comp_telp = $comp_telp;
			$Res->ud_isactive = $isactive;
			
			$Res->ud_create_by = $create_by;
			$Res->ud_create_byfn = $create_byfn;
			$Res->ud_create_at = $create_at;
			$Res->ud_update_by = $create_by;
			$Res->ud_update_byfn = $create_byfn;
			$Res->ud_update_at = $create_at;
			
			$Res->success = 'true';
			$Res->errormessege = '';
			$Res->recnum=$rowidx;
			
		}else
		{
			$Res->success = 'false';
			$Res->errormessege = 'Error Insert';
		}
				
		return response()->json($Res);
		
	}	
	
	public function delete()
	{
		$Res = new DeleteResult();
		$comp_uid = explode(',', Input::get('comp_uid'));
		$tags = array();
		foreach ($comp_id as $key=>$value){
			$tags[$key]=$value;
		}
		$comp = Comp::whereIn('comp_uid', $tags)->delete();	
		if($comp)
		{
			$Res->success = 'true';
			$Res->errormessege = '';
			$Res->deletedid = $comp_uid;
		}else
		{
			$Res->success = 'false';
			$Res->errormessege = 'Error delete';
			$Res->deletedid = $comp_uid;
		}
		return response()->json($Res);
		
	}
	
	
	
}
