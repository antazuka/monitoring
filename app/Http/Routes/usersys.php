<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
Route::get('/regmodule', function(){
	$module = Module::all()->first();
	 echo $module->module_id;
	 echo $module->module_name;
	
});
*/

Route::get('/usersys', 'UserSysController@index');
Route::get('/usersys_getByPage', ['uses'=>'UserSysController@show', 'as' => 'usersys_getByPage']);
Route::post('/usersys_ins', ['uses'=>'UserSysController@insert', 'as' => 'usersys_ins']);
Route::post('/usersys_upd', ['uses'=>'UserSysController@update', 'as' => 'usersys_upd']);
Route::delete('/usersys_del', ['uses'=>'UserSysController@delete', 'as' => 'usersys_del']);
