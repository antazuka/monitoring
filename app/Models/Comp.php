<?php

namespace App\Models;
use Eloquent;
use DB;
use PDO;

class Result
{
	public $selectrow;
	public $maxrow;
	
}

class Comp extends Eloquent {
	
    protected $table = 'comp';
	protected $primaryKey = 'comp_uid';
	public $incrementing = false;

	/* timestamps */
	public $timestamps = true;
	const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';

	public function __construct(){
		parent::__construct();
	}
	
		
	public static function getByPage(
		$var_RowsPerPage,
		$var_PageNum,
		$var_SearchParam)
	{
        $db = DB::connection()->getPdo();
        $stmt = $db->prepare("call sp__comp_getByPage (?,?,?)");
        $stmt->bindParam(1, $var_RowsPerPage);
        $stmt->bindParam(2, $var_PageNum);
		$stmt->bindParam(3, $var_SearchParam);
        $stmt->execute();
        $search = array();
		$Res = new Result();
		$Res->maxrow = $stmt->fetchAll(PDO::FETCH_ASSOC);	
		$stmt->nextRowset();
		$Res->selectrow=$stmt->fetchAll(PDO::FETCH_ASSOC);
         //dd($search); // to see the result
		
		return $Res;		
		
	}
	
	
	
	
	
	
		
}

