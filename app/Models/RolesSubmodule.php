<?php

namespace App\Models;
use Eloquent;
use DB;
use PDO;

class ResultRolesSubmodule
{
	public $selectrow;
	public $maxrow;
	
}

class RolesSubmodule extends Eloquent {
	
    protected $table = 'roles_submodule';
	protected $primaryKey = 'submodule_uid';
	//protected $fillable = array('role_uid', 'submodule_uid');
	public $incrementing = false;

	/* timestamps */
	public $timestamps = true;
	const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';
	
	
	public function Roles() {
        return $this->belongsTo('role_uid');
    }
	
	public function __construct(){
		parent::__construct();
	}
	
		
	
	public static function getByPageSubModule(
		$var_RowsPerPage,
		$var_PageNum,
		$var_SearchParam,
		$var_Role_uid)
	{
        $db = DB::connection()->getPdo();
        $stmt = $db->prepare("call sp__rolessubmodule_GetByPage (?,?,?,?)");
        $stmt->bindParam(1, $var_RowsPerPage);
        $stmt->bindParam(2, $var_PageNum);
		$stmt->bindParam(3, $var_SearchParam);
		$stmt->bindParam(4, $var_Role_uid);
        $stmt->execute();
        $search = array();
		$Res = new ResultRolesSubmodule();
		$Res->maxrow = $stmt->fetchAll(PDO::FETCH_ASSOC);	
		$stmt->nextRowset();
		$Res->selectrow=$stmt->fetchAll(PDO::FETCH_ASSOC);

		
		return $Res;		
		
	}
	
	
	
	
		
}

